let mySwiper = new Swiper('.swiper-container', {
    loop:                 true,
    centeredSlides:       true,
    slidesPerView:        "auto",
    spaceBetween:         30,
    loopAdditionalSlides: 10,
    loopedSlides:         10,
    slideToClickedSlide:  true,
    breakpoints:          {
        1024: {
            loop:                 true,
            centeredSlides:       true,
            slidesPerView:        "auto",
            loopAdditionalSlides: 10,
            loopedSlides:         10,
            slideToClickedSlide:  true,
            spaceBetween:         60
        },
        1880: {
            loop:                 true,
            centeredSlides:       true,
            slidesPerView:        "auto",
            loopAdditionalSlides: 10,
            loopedSlides:         10,
            slideToClickedSlide:  true,
            spaceBetween:         80
        }
    }
});

let mySwiper2 = new Swiper('.reviews__slider-1', {
    loop:                 true,
    slidesPerView:        1,
    loopAdditionalSlides: 10,
    loopedSlides:         10,
    navigation:           {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    }
});

if (document.querySelector(".reviews__slider-1") && document.querySelector(".swiper-container")) {
    mySwiper.controller.control = mySwiper2;
    mySwiper2.controller.control = mySwiper;
}

let mySwiper3 = new Swiper('.mass-media__slider', {
    loop:                 true,
    loopAdditionalSlides: 10,
    loopedSlides:         10,
    spaceBetween:         40,
    slidesPerView:        "auto",
    navigation:           {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },
    breakpoints:          {
        1024: {
            spaceBetween:  0,
            slidesPerView: 4
        }
    }
});

let mySwiper4 = new Swiper('.person__slider-1', {
    loop:                 true,
    loopAdditionalSlides: 10,
    loopedSlides:         10,
    slidesPerView:        1,
    spaceBetween:         20,
    navigation:           {
        nextEl: '.person__button-next',
        prevEl: '.person__button-prev'
    },
    pagination:           {
        el: '.person__dots-2'
    },
    breakpoints:          {
        1024: {
            slidesPerView: 2
        },
        1320: {
            slidesPerView: 3
        }
    }
});

let mySwiper5 = new Swiper('.person__slider-2', {
    loop:                 true,
    loopAdditionalSlides: 10,
    loopedSlides:         10,
    slidesPerView:        1,
    spaceBetween:         20,
    navigation:           {
        nextEl: '.person__button-next',
        prevEl: '.person__button-prev'
    },
    pagination:           {
        el: '.person__dots-2'
    },
    breakpoints:          {
        1024: {
            slidesPerView: 2
        },
        1320: {
            slidesPerView: 3
        }
    }
});

$(document).ready(function () {

    //=====================BURGER=============================================

    $(".header__burger").on("click", function () {
        $(this).toggleClass("active");
        $(".mobile-menu").toggleClass("active");
        $(".header__row").toggleClass("active");
        $("body").toggleClass("lock");
    });

    //=====================TABS================================================

    $(".tab-btn").on("click", function (event) {
        event.preventDefault();

        $(".tab-btn").removeClass("active");
        $(".tab-item").removeClass("active");

        $(this).addClass("active");
        $($(this).attr("href")).addClass("active");
    });
    $(".tab-btn:first").click();

    //=====================IMG TO BACKGROUND CSS================================
    $.each($(".ibg"), function (index, val) {
        if ($(this).find("img").length > 0) {
            $(this).css("background", 'url("' + $(this).find("img").attr("src") + '") no-repeat center / cover');
        }
    });

    //==================PLAY VIDEO==============================================

    let videoCollection = $(".publication-2__wrap");

    $.each(videoCollection, function (index, val) {

        $(".js-prew-1").eq(index).attr("src", $(val).find(".publication-2__prew-image > img").attr("src"));
        $(".js-prew-2").eq(index).attr("src", $(val).find(".publication-2__prew-image > img").attr("src"));

        if (index != 0) {
            $(val).css({"display": "none"});
        }

        if (index == 0) {
            $(".publication-2__discription").find(".publication-2__name").text($(val).find(".publication-2__name").text());
            $(".publication-2__discription").find(".publication-2__text").text($(val).find(".publication-2__text").text());
        }

        if (index != 1) {
            $(".js-prew-1").eq(index).css({"display": "none"});
        }

        if (index != 2) {
            $(".js-prew-2").eq(index).css({"display": "none"});
        }

    });

    $(".js-prew-1").on("click", function () {
        let indexPrew = $(this);

        $.each(videoCollection, function (index, val) {
            if ($(val).is(':visible')) {
                let id = videoCollection.index($(val));
                for (let i = 0; i < $(".js-prew-1").length; i++) {
                    if ($(".js-prew-1").index($(".js-prew-1").eq(i)) == id) {
                        $(".js-prew-1").eq(i).css({"display": "block"});
                    }
                    else {
                        $(".js-prew-1").eq(i).css({"display": "none"});
                    }
                }
            }

            if (index != indexPrew.index()) {
                $(val).css({"display": "none"});
            }
            else {
                $(val).css({"display": "block"});
                $(".publication-2__discription").find(".publication-2__name").text($(val).find(".publication-2__name").text());
                $(".publication-2__discription").find(".publication-2__text").text($(val).find(".publication-2__text").text());
            }
        });
    });

    $(".js-prew-2").on("click", function () {
        let indexPrew = $(this);

        $.each(videoCollection, function (index, val) {
            if ($(val).is(':visible')) {
                let id = videoCollection.index($(val));
                for (let i = 0; i < $(".js-prew-2").length; i++) {
                    if ($(".js-prew-2").index($(".js-prew-2").eq(i)) == id) {
                        $(".js-prew-2").eq(i).css({"display": "block"});
                    }
                    else {
                        $(".js-prew-2").eq(i).css({"display": "none"});
                    }
                }
            }

            if (index != indexPrew.index()) {
                $(val).css({"display": "none"});
            }
            else {
                $(val).css({"display": "block"});
                $(".publication-2__discription").find(".publication-2__name").text($(val).find(".publication-2__name").text());
                $(".publication-2__discription").find(".publication-2__text").text($(val).find(".publication-2__text").text());
            }
        });
    });

    //===============ANIMATION SCROLL======================
    const animItems = $(".anim-items");

    if (animItems.length > 0) {
        $(window).on("scroll", animOnScroll);

        function animOnScroll() {
            $.each(animItems, function (index, val) {
                const animItem = animItems.eq(index);
                const animItemHeight = animItem.innerHeight();
                const animItemOffset = animItem.offset().top;
                const animStart = 10;

                let animItemPoint = $(window).height() - animItemHeight / animStart;

                if (animItemHeight > $(window).height()) {
                    animItemPoint = $(window).height() - $(window).height() / animStart;
                }

                if ($(window).scrollTop() > animItemOffset - animItemPoint && $(window).scrollTop() < animItemOffset + animItemHeight) {
                    animItem.addClass("animate");
                }
                else {
                    if (!animItem.hasClass("anim-no-scrollTop")) {
                        animItem.removeClass("animate");
                    }
                }
            });
        }

        setTimeout(animOnScroll, 0);
    }
    //=========================MACH
    let matchThree = window.matchMedia("(max-width: 1320px)");
    let matchTwo = window.matchMedia("(max-width: 1024px)");
    let match = window.matchMedia("(max-width: 510px)");

    function disabledMobileMenu() {
        if (!matchThree.matches) {
            $(".header__burger").removeClass("active");
            $(".mobile-menu").removeClass("active");
            $(".header__row").removeClass("active");
            $("body").removeClass("lock");
        }
    }

    matchThree.addListener(disabledMobileMenu);
    disabledMobileMenu();
    let mySwiper6;

    function moveEl() {
        if (matchTwo.matches) {
            $.each($(".projects__button"), function (index, val) {
                $(val).parents(".projects__entry").find(".projects__group_discription").append($(val));
            });

            $(".person__group-question").append($(".person__wrap_one"));

            mySwiper6 = new Swiper('.trust__column', {
                loop:           true,
                slidesPerView:  "auto",
                speed:          15000,
                allowTouchMove: false,
                autoplay:       {
                    delay:                0,
                    disableOnInteraction: false
                }

            });
        }
        else {
            $.each($(".projects__button"), function (index, val) {
                $(val).parents(".projects__entry").find(".projects__group_firm").append($(val));
            });

            $(".person__column:last").prepend($(".person__wrap_one"));

            if (mySwiper6) {
                mySwiper6.destroy();
            }
        }
    }

    matchTwo.addListener(moveEl);
    moveEl();

    function moveButton() {
        if (match.matches) {
            $(".person__column:last").before($(".person__button"));
        }
        else {
            $(".person__group-question").prepend($(".person__button"));
        }
    }

    match.addListener(moveButton);
    moveButton();

    let pagination = $(".pagination > ul > li");
    let mySwiper7;

    function SlickActive() {
        if (matchTwo.matches) {

            $.each(pagination, function (index, val) {
                if (!$(val).children().hasClass("prev")
                    && !$(val).children().hasClass("next")
                    && !$(val).find("a").hasClass("current")
                    && !$(val).find("span").length > 0
                    && $(val)[0] != pagination[1]
                    && $(val)[0] != pagination[2]
                    && $(val)[0] != pagination[13]) {
                    $(this).css({"display": "none"});
                }
            });

            mySwiper7 = new Swiper('.services__column', {
                slidesPerView: 1,
                spaceBetween:  20,
                pagination:    {
                    el: '.services__dots'
                }
            });

        }
        else {
            pagination.attr("style", "");

            if (mySwiper7) {
                mySwiper7.destroy(true, true);
            }
        }
    }

    matchTwo.addListener(SlickActive);
    SlickActive();

    //======modal
    $(".js-modal").on("click", function (e) {
        e.preventDefault();
        $(".modal").addClass("active");
        $("body").addClass("lock");
    });

    $(".modal__overlay, .form-modal__close").on("click", function () {
        $(".modal").removeClass("active");
        $("body").removeClass("lock");
    });

    $(".form-modal__service").val($(".service-item__title").text());

    $.each($(".tabs-support__item"), function (index, val) {
        if ($(val).find(".projects__column .projects__entry").length < 4) {
            $(val).find(".pagination-container").css({"display": "none"});
        }
    });

    $.each($(".projects .tab-item"), function (index, val) {
        function simpleTemplating(data) {

            let el = document.createElement("ul");
            $.each(data, function (index, val) {

                el.append(val);
            });

            return el;
        }

        $(val).find('.pagination-container').pagination({
            dataSource: function (done) {
                var result = [];
                for (var i = 0; i < $(val).find(".projects__entry").length; i++) {
                    result.push($(val).find(".projects__entry")[i]);
                }

                done(result);
            },
            pageSize:   3,
            pageRange:  1,
            callback:   function (data, pagination) {

                var html = simpleTemplating(data);

                $(val).find('.data-container').html(html);

                $(val).find(".projects__column").css({"display": "none"});
            }
        });
    });

});

$(document).ready(function () {
    var hash = window.location.hash.substr(1);

    if (hash === "form") {
        var el = document.getElementById("modal-button");
        if (el) {
            el.click();
        }
    }

    if (hash.includes('category-')) {
        var el = document.getElementById(hash + '-tab');
        if (el) {
            el.click();
        }
    }
});
