<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'App\Http\Controllers\MainController@index')->middleware('provideAuthToken');

Route::get('/admin/login', 'App\Http\Controllers\AdminController@auth')
     ->middleware(['provideAuthToken','guest'])
     ->name('admin-login');

Route::post('/form/{type}', 'App\Http\Controllers\FormRequestController@sendForm');

Route::group(['prefix' => 'admin', 'middleware' => ['auth:api']], function ()
{
    Route::get('/', 'App\Http\Controllers\AdminController@index');
    Route::get('{any}', 'App\Http\Controllers\AdminController@index')->where('any', '.*');
});
Route::middleware(['provideAuthToken'])->group(function ()
{
    Route::get('/team/', 'App\Http\Controllers\TeamsController@index');
    Route::get('/team/{id}', 'App\Http\Controllers\TeamsController@show');

    Route::get('/services/', 'App\Http\Controllers\ServicesController@index');
    Route::get('/services/{service}', 'App\Http\Controllers\ServicesController@show')->name('public.services.show');;

    Route::paginate('news', 'App\Http\Controllers\NewsController@index');
    Route::get('/news/{post}', 'App\Http\Controllers\NewsController@show')->name('public.news.show');

    Route::paginate('articles', 'App\Http\Controllers\ArticlesController@index');
    Route::get('/articles/{post}', 'App\Http\Controllers\ArticlesController@show')->name('public.articles.show');

    Route::get('/projects/', 'App\Http\Controllers\ProjectsController@index');
    Route::get('/projects/{id}', 'App\Http\Controllers\ProjectsController@show')->name('public.projects.show');

    Route::get('/contacts/', 'App\Http\Controllers\ContactsController@index');

    Route::paginate('videos', 'App\Http\Controllers\VideosController@index');
    Route::get('/videos/{video}', 'App\Http\Controllers\VideosController@show')->name('public.videos.show');

    Route::get('/vacancies/', 'App\Http\Controllers\VacanciesController@index');

    Route::get('/clients/', 'App\Http\Controllers\ClientsController@index');

    Route::paginate('search', 'App\Http\Controllers\SearchController@index');

    Route::get('/{page}', 'App\Http\Controllers\PagesController@show')->where('page', '.*');
});
