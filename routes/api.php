<?php

use App\Http\Controllers\Api\v1\ArticlesController;
use App\Http\Controllers\Api\v1\DocumentController;
use App\Http\Controllers\Api\v1\ExpertLinkController;
use App\Http\Controllers\Api\v1\FormRequestsController;
use App\Http\Controllers\Api\v1\NewsController;
use App\Http\Controllers\Api\v1\ProfileController;
use App\Http\Controllers\Api\v1\ProjectsCategoryController;
use App\Http\Controllers\Api\v1\ReviewController;
use App\Http\Controllers\Api\v1\ServiceController;
use App\Http\Controllers\Api\v1\ServicesCategoryController;
use App\Http\Controllers\Api\v1\SiteInfoController;
use App\Http\Controllers\Api\v1\SocialMediasController;
use App\Http\Controllers\Api\v1\UsersController;
use App\Http\Controllers\Api\v1\ProjectController;
use App\Http\Controllers\Api\v1\VideoController;
use App\Http\Controllers\Api\v1\ClientController;
use App\Http\Controllers\Api\v1\ServicesDirectionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'App\Http\Controllers\AuthController@login');
Route::post('register', 'App\Http\Controllers\AuthController@register');

Route::group(['middleware' => 'auth:api'], function ()
{
    Route::post('logout', 'App\Http\Controllers\AuthController@logout');
    Route::get('user', 'App\Http\Controllers\AuthController@getUserInfo');

    Route::resource('clients', ClientController::class);
    Route::resource('documents', DocumentController::class);
    Route::resource('site-info', SiteInfoController::class);
    Route::resource('news', NewsController::class);
    Route::resource('articles', ArticlesController::class);
    Route::resource('users', UsersController::class);
    Route::resource('profiles', ProfileController::class);
    Route::resource('form-requests', FormRequestsController::class);
    Route::resource('projects', ProjectController::class);
    Route::resource('projects-categories', ProjectsCategoryController::class);
    Route::resource('videos', VideoController::class);
    Route::resource('reviews', ReviewController::class);
    Route::resource('expert-links', ExpertLinkController::class);
    Route::resource('social-medias', SocialMediasController::class);
    Route::resource('services', ServiceController::class);
    Route::resource('services-categories', ServicesCategoryController::class);
    Route::resource('services-directions', ServicesDirectionController::class);

    Route::post('file-upload/{bundle}', 'App\Http\Controllers\FileUploadController@fileUpload');
});
