php artisan down || true
git pull origin master
composer install --no-interaction --prefer-dist --optimize-autoloader --no-dev

php artisan migrate --seed --force

php artisan storage:link

cp ./resources/img/site_info_defaults/. ./public/storage/site_info
cp ./resources/img/services_directions_defaults/. ./public/storage/services

php artisan passport:install
php artisan passport:keys


php artisan cache:clear
php artisan route:cache
php artisan config:cache
php artisan view:cache

npm ci

npm run production

php artisan up
