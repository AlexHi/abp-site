<?php

namespace App\Http\Middleware;

use Closure;

class ProvideAuthToken
{

    public function handle($request, Closure $next)
    {

        if ($request->cookie('auth_token'))
        {
            $request->headers->set('Authorization', 'Bearer ' . $request->cookie('auth_token'));
        }

        return $next($request);
    }

}
