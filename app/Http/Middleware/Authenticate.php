<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Cookie;

class Authenticate extends Middleware
{

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson())
        {
            return route('admin-login');
        }
    }

    public function handle($request, Closure $next, ...$guards)
    {

        if ($request->cookie('auth_token'))
        {
            $request->headers->set('Authorization', 'Bearer ' . $request->cookie('auth_token'));
        }

        $this->authenticate($request, $guards);

        return $next($request);
    }

}
