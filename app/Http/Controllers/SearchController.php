<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Project;
use App\Models\Service;
use App\Models\Video;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;

class SearchController extends Controller
{

    public function index(Request $request)
    {
        $items      = [];
        $searchText = $request->get('search');
        //$items      = Post::query()->where('active', true)
        //                  ->latest()->paginate();

        if ($searchText)
        {
            $items = (new Search())
                ->registerModel(Post::class, ['title', 'description', 'text'])
                ->registerModel(Project::class, ['title', 'description', 'text'])
                ->registerModel(Service::class, ['name', 'text'])
                ->registerModel(Video::class, ['name', 'description'])
                ->perform($searchText);
        }

        return view('public.search.index', [
            'items'       => $items,
            'search_text' => $searchText
        ]);
    }
}
