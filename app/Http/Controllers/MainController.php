<?php

namespace App\Http\Controllers;

use App\Models\ExpertLink;
use App\Models\Post;
use App\Models\Review;
use App\Models\ServicesDirection;
use App\Models\Video;
use Illuminate\Support\Facades\Cache;

class MainController extends Controller
{

    public function index()
    {
        $data = Cache::remember('main.page.data', 3600, function ()
        {
            $videos             = Video::query()->where('active', true)->latest()->take(3)->get();
            $reviews            = Review::query()->where('active', true)->latest()->get();
            $expertLinks        = ExpertLink::query()->where('active', true)->latest()->get();
            $servicesDirections = ServicesDirection::query()->with('category')->where('active', true)->orderBy('sort')
                                                   ->get();
            $news               = Post::query()->where('active', true)->where('type', 'news')
                                      ->latest()->take(5)->get();
            $articles           = Post::query()->where('active', true)->where('type', 'articles')
                                      ->latest()->take(8)
                                      ->get();
            return [
                'publish'            => [
                    'news'     => $news,
                    'articles' => $articles,
                    'videos'   => $videos
                ],
                'servicesDirections' => $servicesDirections,
                'reviews'            => $reviews,
                'expert_links'       => $expertLinks
            ];
        });

        return view('public.main', $data);
    }
}
