<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Support\Facades\Cache;

class ClientsController extends Controller
{

    public function index()
    {
        $clients = Cache::remember('clients.page.data', 3600, function ()
        {
            return Client::query()->where('active', true)->latest()->get();
        });

        return view('public.clients.index', compact('clients'));
    }

}
