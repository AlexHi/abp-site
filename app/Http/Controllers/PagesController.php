<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{

    public function show(Page $page)
    {
        return view('public.pages.index', compact('page'));
    }
}
