<?php

namespace App\Http\Controllers;

use App\Models\User;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function getUserInfo(Request $request)
    {
        return $this->success(
            User::query()
                ->whereKey($request->user()->id)
                ->with('profile')
                ->first()
                ->toArray()
        );
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails())
        {
            return $this->error('Неверные данные', ['errors' => $validator->errors()->all()]);
        }

        $user = User::query()->where('email', $request->username)->first();

        if ($user)
        {
            if (Hash::check($request->password, $user->password))
            {
                $token    = $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = ['token' => $token];
                return $this->success($response)->withCookie(Cookie::make('auth_token', $token, 6000));
            }
            else
            {
                return $this->error("Неверный пароль");
            }
        }
        else
        {
            return $this->error('Пользователь не наден');
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required|string|max:255',
            'email'    => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'type'     => 'integer',
        ]);
        if ($validator->fails())
        {
            return response(['errors' => $validator->errors()->all()], 422);
        }
        $request['type']           = $request['type'] ? $request['type'] : 0;
        $request['password']       = Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        $user                      = User::create($request->toArray());
        $token                     = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response                  = ['token' => $token];
        return response($response, 200);
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->delete();
        Cookie::queue(Cookie::forget('auth_token'));
        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }
}
