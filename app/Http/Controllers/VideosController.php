<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;

class VideosController extends Controller
{

    public function index()
    {
        $videos = Video::query()->where('active', true)->latest()->paginate(10);

        return view('public.videos.index', ['videos' => $videos]);
    }

    public function show(Video $video)
    {
        setlocale(LC_TIME, 'ru_RU.UTF-8');

        $posts = Video::query()->where('active', true)->latest()->take(4)->get();

        return view('public.videos.show', [
            'video'  => $video,
            'videos' => $posts
        ]);
    }
}
