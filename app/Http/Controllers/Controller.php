<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Validation\Validator;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public final function error($desc, $data = [])
    {
        return response([
            'status' => 'failed',
            'data'   => $data,
            'desc'   => trim($desc),
        ], 200);
    }

    public final function success(array $data = [], $total = 0)
    {
        return response([
            'status' => 'success',
            'data'   => $data,
            'total'  => $total
        ], 200);
    }
}
