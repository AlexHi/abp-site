<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\ServicesCategory;

class ServicesController extends Controller
{

    public function index()
    {
        $servicesCategories = ServicesCategory::query()->where('active', true)->with([
            'services' => function ($query)
            {
                $query->where('active', true);
            }
        ])->orderByDesc('sort')->get();

        return view('public.services.index', ['services_categories' => $servicesCategories]);
    }

    public function show(Service $service)
    {
        $totalPriceElements = $service->total_price ? explode("/", $service->total_price) : [];

        return view('public.services.show', ['service' => $service, 'total_price_elements' => $totalPriceElements]);
    }
}
