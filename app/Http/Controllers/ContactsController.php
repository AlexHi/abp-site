<?php

namespace App\Http\Controllers;

use App\Models\SiteInfo;
use App\Models\User;
use Illuminate\Http\Request;

class ContactsController extends Controller
{

    public function index()
    {
        $siteInfo = SiteInfo::query()->where('lang', 'ru')->first();

        $contactUser = null;

        if ($siteInfo->contact_user_id)
        {
            $contactUser = User::query()->whereKey($siteInfo->contact_user_id)->with('profile')->first();
        }

        return view('public.contacts.index', [
            'site_info'    => $siteInfo,
            'contact_user' => $contactUser
        ]);
    }
}
