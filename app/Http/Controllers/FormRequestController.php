<?php

namespace App\Http\Controllers;

use App\Models\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class FormRequestController extends Controller
{

    /**
     * Get the error messages for the defined validation rules.
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'         => __('forms_messages.name_required'),
            'email.required'        => __('forms_messages.email_required'),
            'phone_number.required' => __('forms_messages.phone_required'),
            'phone_number.regex'    => __('forms_messages.phone_format'),
            'phone_number.min'      => __('forms_messages.phone_format'),
            'message.required'      => __('forms_messages.message_required'),
            'agreement.accepted'    => __('forms_messages.agreement_accepted')
        ];
    }

    public function sendForm(Request $request, string $type)
    {
        $method = lcfirst(Str::camel($type));

        if (!method_exists($this, $method)) {
            return abort(403, 'Method not allowed');
        }

        $this->$method($request);

        return Redirect::to(URL::previous() . "#form")->with('success', __('forms_messages.success'));
    }

    private function questions(Request $request)
    {
        $request->validate([
            'name'         => 'required',
            'email'        => 'required|email',
            'phone_number' => '',
            //'message'      => 'required',
            'agreement'    => 'accepted'
        ], $this->messages());

        $formRequest = new FormRequest();

        $formRequest->form_name    = 'Остались вопросы';
        $formRequest->name         = $request->name;
        $formRequest->email        = $request->email;
        $formRequest->phone_number = $request->phone_number;
        $formRequest->message      = (isset($request->message)) ? $request->message : '';

        $formRequest->save();

        $subject = 'Заполнена форма "Остались вопросы"';
        $order   = '';
        if (!empty($_COOKIE['utm'])) {
            foreach ($_COOKIE['utm'] as $i => $row) {
                $order .= $i . ': ' . $row . "\r\n";
            }
        }

        Mail::send('email.questions',
            [
                'id'           => $formRequest->id,
                'name'         => $request->name,
                'email'        => $request->email,
                'phone_number' => $request->phone_number,
                'user_message' => $request->message,
                'site_url'     => config('app.url'),
                'subject'      => $subject,
                'utm'          => $order
            ], function ($message) use ($request, $subject)
            {
                $message->from(config('mail.from.address'), config('mail.from.name'));
                $message->to('abp@abp.legal')->subject($subject);
            });
    }

    private function expertQuestions(Request $request)
    {
        $request->validate([
            'name'         => 'required',
            'email'        => 'required|email',
            'phone_number' => '',
            'message'      => 'required',
            'agreement'    => 'accepted'
        ], $this->messages());

        $formRequest = new FormRequest();

        $formRequest->form_name    = 'Задать вопрос эксперту';
        $formRequest->name         = $request->name;
        $formRequest->email        = $request->email;
        $formRequest->phone_number = $request->phone_number;
        $formRequest->message      = $request->message;

        $formRequest->save();

        $subject = 'Заполнена форма "Задать вопрос эксперту"';
        if (!empty($_COOKIE['utm'])) {
            foreach ($_COOKIE['utm'] as $i => $row) {
                $order .= $i . ': ' . $row . "\r\n";
            }
        }
        Mail::send('email.expert_questions',
            [
                'id'           => $formRequest->id,
                'name'         => $request->name,
                'email'        => $request->email,
                'phone_number' => $request->phone_number,
                'user_message' => $request->message,
                'site_url'     => config('app.url'),
                'subject'      => $subject,
                'utm'          => $order
            ], function ($message) use ($request, $subject)
            {
                $message->from(config('mail.from.address'), config('mail.from.name'));
                $message->to('abp@abp.legal')->subject($subject);
            });
    }

    private function teamsExpertQuestion(Request $request)
    {
        $request->validate([
            'name'         => 'required',
            'email'        => 'required|email',
            'phone_number' => '',
            'thing_name'   => '',
            'thing_url'    => '',
            'user_email'   => '',
            'message'      => 'required',
            'agreement'    => 'accepted'
        ], $this->messages());

        $formRequest = new FormRequest();

        $formRequest->form_name    = 'Задать вопрос эксперту';
        $formRequest->name         = $request->name;
        $formRequest->email        = $request->email;
        $formRequest->phone_number = $request->phone_number;
        $formRequest->message      = $request->message;
        $formRequest->thing_name   = $request->thing_name;
        $formRequest->thing_url    = $request->thing_url;

        $formRequest->save();

        $subject = 'Заполнена форма "Задать вопрос эксперту" у пользователя ' . $request->thing_name;
        if (!empty($_COOKIE['utm'])) {
            foreach ($_COOKIE['utm'] as $i => $row) {
                $order .= $i . ': ' . $row . "\r\n";
            }
        }
        Mail::send('email.teams_expert_question',
            [
                'id'           => $formRequest->id,
                'name'         => $request->name,
                'email'        => $request->email,
                'phone_number' => $request->phone_number,
                'user_message' => $request->message,
                'thing_name'   => $request->thing_name,
                'thing_url'    => $request->thing_url,
                'site_url'     => config('app.url'),
                'subject'      => $subject,
                'utm'          => $order
            ], function ($message) use ($request, $subject)
            {
                $message->from(config('mail.from.address'), config('mail.from.name'));
                $message->to('abp@abp.legal')->subject($subject);
                $message->cc($request->user_email);
            });
    }

    private function serviceOrder(Request $request)
    {
        $request->validate([
            'name'         => 'required',
            'email'        => 'required|email',
            'phone_number' => '',
            'thing_name'   => '',
            'thing_url'    => '',
            'message'      => 'required',
            'agreement'    => 'accepted'
        ], $this->messages());

        $formRequest = new FormRequest();

        $formRequest->form_name    = 'Заказать услугу';
        $formRequest->name         = $request->name;
        $formRequest->email        = $request->email;
        $formRequest->phone_number = $request->phone_number;
        $formRequest->message      = $request->message;
        $formRequest->thing_name   = $request->thing_name;
        $formRequest->thing_url    = $request->thing_url;

        $formRequest->save();

        $subject = 'Заполнена форма "Заказать услугу" с услугой "' . $request->thing_name . '"';
        if (!empty($_COOKIE['utm'])) {
            foreach ($_COOKIE['utm'] as $i => $row) {
                $order .= $i . ': ' . $row . "\r\n";
            }
        }
        Mail::send('email.service_order',
            [
                'id'           => $formRequest->id,
                'name'         => $request->name,
                'email'        => $request->email,
                'phone_number' => $request->phone_number,
                'user_message' => $request->message,
                'thing_name'   => $request->thing_name,
                'thing_url'    => $request->thing_url,
                'site_url'     => config('app.url'),
                'subject'      => $subject,
                'utm'          => $order
            ], function ($message) use ($request, $subject)
            {
                $message->from(config('mail.from.address'), config('mail.from.name'));
                $message->to('abp@abp.legal')->subject($subject);
            });
    }

    private function phone(Request $request)
    {
        $request->validate([
            'name'         => '',
            'email'        => '',
            'phone_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'thing_name'   => '',
            'thing_url'    => '',
            'user_email'   => '',
            'message'      => '',
            'agreement'    => ''
        ], $this->messages());

        $formRequest = new FormRequest();

        $formRequest->form_name    = 'Закажи консультацию сейчас';
        $formRequest->phone_number = $request->phone_number;
        $formRequest->name         = 'Не заполнено';
        $formRequest->email        = 'Не заполнено';
        $formRequest->message      = 'Не заполнено';
        $formRequest->save();

        $order   = '';
        $subject = 'Заполнена форма "Закажи консультацию сейчас"';
        if (!empty($_COOKIE['utm'])) {
            foreach ($_COOKIE['utm'] as $i => $row) {
                $order .= $i . ': ' . $row . "\r\n";
            }
        }
        Mail::send('email.phone',
            [
                'id'           => $formRequest->id,
                'phone_number' => $request->phone_number,
                'site_url'     => config('app.url'),
                'subject'      => $subject,
                'utm'          => $order
            ], function ($message) use ($request, $subject)
            {
                $message->from(config('mail.from.address'), config('mail.from.name'));
                $message->to('abp@abp.legal')->subject($subject);
            });
    }
}
