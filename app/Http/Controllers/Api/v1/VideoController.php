<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use function PHPUnit\Framework\isEmpty;

class VideoController extends Controller
{

    public function messages()
    {
        return [
            'name.required'        => 'Пожалуйста, введите название.',
            'link.required'        => 'Пожалуйста, добавьте ссылку.',
            'description.required' => 'Пожалуйста, введите описание.',
        ];
    }

    private function forgetCache()
    {
        Cache::forget('main.page.data');
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsQuery = Video::query();

        if ($request->get('count'))
        {
            if ($request->get('page'))
            {
                $itemsQuery->offset(($request->get('page') - 1) * $request->get('count'));
            }
            $itemsQuery->take($request->get('count'));
        }

        return $this->success(
            $itemsQuery->get()
                ->toArray(),
            Video::query()
                ->count()
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'link'        => 'required',
            'name'        => 'required',
            'description' => 'required',
        ], $this->messages());

        $codes = $this->getYoutubeCode($data['link']);

        if (count($codes) === 0)
        {
            return $this->error("Проверьте правильность введенной ссылки - поддерживаются только ссылки с сайта youtube.com");
        }

        $youtubeCode = $codes[0];

        $video = Video::create([
            'name'         => $data['name'],
            'description'  => $data['description'],
            'link'         => $data['link'],
            'youtube_code' => $youtubeCode
        ]);

        $this->forgetCache();

        return $this->success($video->toArray());
    }

    private function getYoutubeCode(string $link)
    {
        $matches = [];

        preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $link, $matches);

        return $matches;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Video $video
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        return $this->success($video->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Video $video
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Video        $video
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        $data = $request->validate([
            'active'      => '',
            'link'        => 'required',
            'name'        => 'required',
            'description' => 'required',
        ], $this->messages());

        $codes = $this->getYoutubeCode($data['link']);

        if (count($codes) === 0)
        {
            return $this->error("Проверьте правильность введенной ссылки - поддерживаются только ссылки с сайта youtube.com");
        }

        $youtubeCode = $codes[0];

        $video->active       = $data['active'] === 'true' || $data['active'] === '1';
        $video->name         = $data['name'];
        $video->description  = $data['description'];
        $video->link         = $data['link'] ?? '';
        $video->youtube_code = $youtubeCode;

        $video->save();

        $this->forgetCache();

        return $this->success($video->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Video $video
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        $video->delete();
        $this->forgetCache();
        return $this->success(['deleted']);
    }
}
