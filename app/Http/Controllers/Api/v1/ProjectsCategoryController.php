<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\ProjectsCategory;
use App\Models\ServicesCategory;
use Illuminate\Http\Request;

class ProjectsCategoryController extends Controller
{

    public function messages()
    {
        return [
            'name.required' => 'Пожалуйста, введите название.',
            'sort.required' => 'Пожалуйста, введите сортировку.',
            'icon.required' => 'Пожалуйста, добавьте иконку.',
            'icon.image'    => 'Загруженная иконка не является изображением',
        ];
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->success(ProjectsCategory::query()->orderByDesc('sort')->get()->toArray());
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'sort' => 'required',
            'icon' => ['required', 'image']
        ], $this->messages());

        $imagePath = $data['icon']->store('services', 'public');

        $projectsCategory = new ProjectsCategory();

        $projectsCategory->name = $data['name'];
        $projectsCategory->sort = $data['sort'];
        $projectsCategory->icon = $imagePath;

        $projectsCategory->save();

        return $this->success($projectsCategory->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\ProjectsCategory $projectsCategory
     *
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectsCategory $projectsCategory)
    {
        return $this->success($projectsCategory->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ProjectsCategory $projectsCategory
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectsCategory $projectsCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request     $request
     * @param \App\Models\ProjectsCategory $projectsCategory
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectsCategory $projectsCategory)
    {
        $data = $request->validate([
            'active' => '',
            'name'   => 'required',
            'sort'   => 'required',
            'icon'   => ''
        ], $this->messages());

        if ($data['icon'])
        {
            $imagePath = $data['icon']->store('services', 'public');
            if ($imagePath)
            {
                $projectsCategory->icon = $imagePath;
            }
        }

        $projectsCategory->active = $data['active'] === 'true' || $data['active'] === '1';
        $projectsCategory->name   = $data['name'];
        $projectsCategory->sort   = $data['sort'];

        $projectsCategory->save();

        return $this->success($projectsCategory->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ProjectsCategory $projectsCategory
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectsCategory $projectsCategory)
    {
        $projectsCategory->delete();
        return $this->success(['deleted']);
    }
}
