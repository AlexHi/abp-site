<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ClientController extends Controller
{

    public function messages()
    {
        return [
            'name.required'        => 'Пожалуйста, введите название.',
            'description.required' => 'Пожалуйста, введите описание.',
            'logo.required'        => 'Пожалуйста, добавьте логотип.',
            'logo.image'           => 'Загруженный логотип не является изображением.',
        ];
    }

    private function forgetCache()
    {
        Cache::forget('clients.page.data');
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsQuery = Client::query();

        if ($request->get('count'))
        {
            if ($request->get('page'))
            {
                $itemsQuery->offset(($request->get('page') - 1) * $request->get('count'));
            }
            $itemsQuery->take($request->get('count'));
        }

        return $this->success(
            $itemsQuery->get()
                ->toArray(),
            Client::query()
                ->count()
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'link'        => '',
            'name'        => 'required',
            'description' => 'required',
            'logo'        => ['required', 'image']
        ], $this->messages());

        $imagePath = $data['logo']->store('clients', 'public');

        $client = Client::create([
            'name'        => $data['name'],
            'description' => $data['description'],
            'link'        => $data['link'] ?? '',
            'logo'        => $imagePath
        ]);

        $this->forgetCache();

        return $this->success($client->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Client $client
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return $this->success($client->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Client $client
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Client       $client
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $data = $request->validate([
            'active'      => '',
            'link'        => '',
            'name'        => 'required',
            'description' => 'required',
            'logo'        => ['required', 'image']
        ], $this->messages());

        $client->active      = $data['active'] === 'true' || $data['active'] === '1';
        $client->name        = $data['name'];
        $client->description = $data['description'];
        $client->link        = $data['link'] ?? '';

        if ($data['logo'])
        {
            $client->logo = $data['logo']->store('clients', 'public');
        }

        $client->save();

        $this->forgetCache();

        return $this->success($client->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Client $client
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();
        $this->forgetCache();
        return $this->success(['deleted']);
    }
}
