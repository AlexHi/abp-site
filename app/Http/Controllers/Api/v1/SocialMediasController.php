<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\SiteInfo;
use App\Models\SocialMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class SocialMediasController extends Controller
{

    public function messages()
    {
        return [
            'name.required' => 'Пожалуйста, введите название.',
            'link.required' => 'Пожалуйста, добавьте ссылку.',
            'logo.required' => 'Пожалуйста, добавьте логотип.',
            'logo.image'    => 'Загруженный логотип не является изображением.',
        ];
    }

    private function forgetCache()
    {
        Cache::forget('main.page.data');

        $siteInfos = SiteInfo::query()
            ->get();

        foreach ($siteInfos as $siteInfo)
        {
            Cache::forget('site_info.' . $siteInfo->lang);
        }
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsQuery = SocialMedia::query();

        if ($request->get('count'))
        {
            if ($request->get('page'))
            {
                $itemsQuery->offset(($request->get('page') - 1) * $request->get('count'));
            }
            $itemsQuery->take($request->get('count'));
        }

        return $this->success(
            $itemsQuery->get()
                ->toArray(),
            SocialMedia::query()
                ->count()
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'link' => 'required',
            'logo' => ['required']
        ], $this->messages());

        $logoOriginalName = $data['logo']->getClientOriginalName();

        $logoPath = $data['logo']->storeAs('public/site_info', $logoOriginalName);

        $document = SocialMedia::create([
            'site_info_id' => 1,
            'name'         => $data['name'],
            'link'         => $data['link'],
            'logo'         => str_replace('public/', '', $logoPath)
        ]);

        $this->forgetCache();

        return $this->success($document->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(SocialMedia $socialMedia)
    {
        return $this->success($socialMedia->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SocialMedia $socialMedia)
    {
        $data = $request->validate([
            'active' => '',
            'name'   => 'required',
            'link'   => 'required',
            'logo'   => ['required']
        ], $this->messages());

        $fileOriginalName = $data['logo']->getClientOriginalName();

        $filePath = $data['logo']->storeAs('public/site_info', $fileOriginalName);

        $socialMedia->active = $data['active'] === 'true' || $data['active'] === '1';
        $socialMedia->name   = $data['name'];
        $socialMedia->link   = $data['link'];
        $socialMedia->logo   = str_replace('public/', '', $filePath);

        $socialMedia->save();

        $this->forgetCache();

        return $this->success($socialMedia->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SocialMedia::destroy($id);
        $this->forgetCache();
        return $this->success(['deleted']);
    }
}
