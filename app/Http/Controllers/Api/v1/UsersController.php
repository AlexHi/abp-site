<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{

    public function messages()
    {
        return [
            'name.required'      => 'Пожалуйста, введите имя.',
            'last_name.required' => 'Пожалуйста, введите фамилию.',
            'email.required'     => 'Пожалуйста, введите email.',
            'password.required'  => 'Пожалуйста, введите пароль.',
        ];
    }

    private function checkUserExists($email, $userId = 0)
    {
        $checkUser = User::query()
            ->where('email', $email)
            ->first();

        if ($checkUser && $userId !== $checkUser->id)
        {
            return true;
        }
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsQuery = User::query()
            ->with('profile');
        if ($request->get('count'))
        {
            if ($request->get('page'))
            {
                $itemsQuery->offset(($request->get('page') - 1) * $request->get('count'));
            }
            $itemsQuery->take($request->get('count'));
        }

        return $this->success(
            $itemsQuery->get()
                ->toArray(),
            User::query()
                ->count()
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'          => 'required',
            'last_name'     => 'required',
            'email'         => 'required',
            'show_in_teams' => '',
            'password'      => 'required'
        ], $this->messages());

        if ($this->checkUserExists($data['email']))
        {
            return $this->error('Пользователь с таким email уже существует');
        }

        $user = new User();

        $user->name          = $data['name'];
        $user->last_name     = $data['last_name'];
        $user->email         = $data['email'];
        $user->show_in_teams = ($data['show_in_teams'] ?? null) && $data['show_in_teams'] === "true" ? 1 : 0;
        $user->password      = Hash::make($data['password']);

        $user->save();

        return $this->success($user->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $this->success($user->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = $request->validate([
            'name'          => 'required',
            'last_name'     => 'required',
            'email'         => 'required',
            'show_in_teams' => '',
            'password'      => ''
        ], $this->messages());

        if ($this->checkUserExists($data['email'], $user->id))
        {
            return $this->error('Пользователь с таким email уже существует');
        }

        $user->name          = $data['name'];
        $user->last_name     = $data['last_name'];
        $user->email         = $data['email'];
        $user->show_in_teams = $data['show_in_teams'] === "true" ? 1 : 0;

        if ($data['password'] ?? null)
        {
            $user->password = Hash::make($data['password']);
        }

        $user->save();

        return $this->success($user->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        Profile::query()
            ->where('user_id', $id)
            ->delete();
        return $this->success(['deleted']);
    }
}
