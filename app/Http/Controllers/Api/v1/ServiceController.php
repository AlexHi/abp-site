<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    public function messages()
    {
        return [
            'name.required'                 => 'Пожалуйста, введите название.',
            'minimal_price.required'        => 'Пожалуйста, введите минимальную стоимость.',
            'price_note.required'           => 'Пожалуйста, введите примечание цены.',
            'total_price.required'          => 'Пожалуйста, введите итоговую цену.',
            'text.required'                 => 'Пожалуйста, введите текст.',
            'services_category_id.required' => 'Пожалуйста, выберите категорию.',
        ];
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $servicesQuery = Service::query();

        if ($request->get('category_id'))
        {
            $servicesQuery->where('services_category_id', $request->get('category_id'));
        }

        $services = $servicesQuery->get();
        return $this->success($services->toArray());
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'                 => 'required',
            'minimal_price'        => 'required',
            'price_note'           => 'required',
            'total_price'          => 'required',
            'text'                 => 'required',
            'services_category_id' => 'required',
            'steps'                => '',
            'sub_services'         => ''
        ], $this->messages());

        $service = new Service();

        $service->name                 = $data['name'];
        $service->minimal_price        = $data['minimal_price'];
        $service->price_note           = $data['price_note'];
        $service->total_price          = $data['total_price'];
        $service->text                 = $data['text'];
        $service->steps                = $data['steps'];
        $service->sub_services         = $data['sub_services'];
        $service->services_category_id = $data['services_category_id'];

        $service->save();

        return $this->success($service->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Service $service
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        return $this->success($service->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Service $service
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Service      $service
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $data = $request->validate([
            'active'               => '',
            'name'                 => 'required',
            'minimal_price'        => 'required',
            'price_note'           => 'required',
            'total_price'          => 'required',
            'text'                 => 'required',
            'services_category_id' => 'required',
            'steps'                => '',
            'sub_services'         => ''
        ], $this->messages());

        $service->active               = $data['active'] === 'true' || $data['active'] === '1';
        $service->name                 = $data['name'];
        $service->minimal_price        = $data['minimal_price'];
        $service->price_note           = $data['price_note'];
        $service->total_price          = $data['total_price'];
        $service->text                 = $data['text'];
        $service->steps                = $data['steps'];
        $service->sub_services         = $data['sub_services'];
        $service->services_category_id = $data['services_category_id'];

        $service->save();

        return $this->success($service->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Service $service
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return $this->success(['deleted']);
    }
}
