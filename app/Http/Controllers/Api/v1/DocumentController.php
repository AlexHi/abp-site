<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Models\SiteInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class DocumentController extends Controller
{

    public function messages()
    {
        return [
            'name.required' => 'Пожалуйста, введите название.',
            'file.required' => 'Пожалуйста, добавьте файл.',
        ];
    }

    private function forgetCache()
    {
        $siteInfos = SiteInfo::query()
            ->get();

        foreach ($siteInfos as $siteInfo)
        {
            Cache::forget('site_info.' . $siteInfo->lang);
        }
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsQuery = Document::query();

        if ($request->get('count'))
        {
            if ($request->get('page'))
            {
                $itemsQuery->offset(($request->get('page') - 1) * $request->get('count'));
            }
            $itemsQuery->take($request->get('count'));
        }

        return $this->success(
            $itemsQuery->get()
                ->toArray(),
            Document::query()
                ->count()
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'file' => ['required']
        ], $this->messages());

        $fileOriginalName = $data['file']->getClientOriginalName();

        $filePath = $data['file']->storeAs('public/documents', $fileOriginalName);

        $document = Document::create([
            'name' => $data['name'],
            'file' => str_replace('public/', '', $filePath)
        ]);

        $this->forgetCache();

        return $this->success($document->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Document $document
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        return $this->success($document->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Document $document
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Document     $document
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        $data = $request->validate([
            'name' => 'required',
            'file' => ['required']
        ], $this->messages());

        $fileOriginalName = $data['file']->getClientOriginalName();

        $filePath = $data['file']->storeAs('public/documents', $fileOriginalName);

        $document->name = $data['name'];
        $document->file = str_replace('public/', '', $filePath);

        $document->save();

        $this->forgetCache();

        return $this->success($document->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Document $document
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        $document->delete();
        $this->forgetCache();
        return $this->success(['deleted']);
    }
}
