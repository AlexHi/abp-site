<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\ServicesDirection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ServicesDirectionController extends Controller
{

    public function messages()
    {
        return [
            'name.required'          => 'Пожалуйста, введите название.',
            'services_list.required' => 'Пожалуйста, добавьте хотя бы одну услугу.',
            'sort.required'          => 'Пожалуйста, введите сортировку.',
            'icon.required'          => 'Пожалуйста, добавьте иконку.',
            'icon.image'             => 'Загруженная иконка не является изображением',
        ];
    }

    private function forgetCache()
    {
        Cache::forget('main.page.data');
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsQuery = ServicesDirection::query()
            ->orderByDesc('sort');

        if ($request->get('count'))
        {
            if ($request->get('page'))
            {
                $itemsQuery->offset(($request->get('page') - 1) * $request->get('count'));
            }
            $itemsQuery->take($request->get('count'));
        }

        return $this->success(
            $itemsQuery->get()
                ->toArray(),
            ServicesDirection::query()
                ->count()
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'                 => 'required',
            'services_list'        => 'required',
            'sort'                 => 'required',
            'link'                 => '',
            'services_category_id' => '',
            'icon'                 => ['image', 'required']
        ], $this->messages());

        $imagePath = $data['icon']->store('services', 'public');

        $servicesDirection = new ServicesDirection();

        $servicesDirection->name          = $data['name'];
        $servicesDirection->icon          = $imagePath;
        $servicesDirection->sort          = $data['sort'];
        $servicesDirection->services_list = $data['services_list'];

        if (isset($data['link']))
        {
            $servicesDirection->link = $data['link'];
        }

        if (isset($data['services_category_id']))
        {
            $servicesDirection->services_category_id = $data['services_category_id'];
        }

        $servicesDirection->save();

        $this->forgetCache();

        return $this->success($servicesDirection->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\ServicesDirection $servicesDirection
     *
     * @return \Illuminate\Http\Response
     */
    public function show(ServicesDirection $servicesDirection)
    {
        return $this->success($servicesDirection->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ServicesDirection $servicesDirection
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(ServicesDirection $servicesDirection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request      $request
     * @param \App\Models\ServicesDirection $servicesDirection
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServicesDirection $servicesDirection)
    {
        $data = $request->validate([
            'active'               => '',
            'name'                 => 'required',
            'sort'                 => 'required',
            'services_list'        => 'required',
            'link'                 => '',
            'services_category_id' => '',
            'icon'                 => ''
        ], $this->messages());

        if ($data['icon'])
        {
            $imagePath = $data['icon']->store('services', 'public');
            if ($imagePath)
            {
                $servicesDirection->icon = $imagePath;
            }
        }

        $servicesDirection->active        = $data['active'] === 'true' || $data['active'] === '1';
        $servicesDirection->name          = $data['name'];
        $servicesDirection->sort          = $data['sort'];
        $servicesDirection->services_list = $data['services_list'];

        if (isset($data['link']))
        {
            $servicesDirection->link = $data['link'];
        }

        if (isset($data['services_category_id']))
        {
            $servicesDirection->services_category_id = $data['services_category_id'];
        }

        $servicesDirection->save();

        $this->forgetCache();

        return $this->success($servicesDirection->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ServicesDirection $servicesDirection
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServicesDirection $servicesDirection)
    {
        $servicesDirection->delete();
        $this->forgetCache();
        return $this->success(['deleted']);
    }
}
