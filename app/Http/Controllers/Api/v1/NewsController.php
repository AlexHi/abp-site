<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class NewsController extends Controller
{

    public function messages()
    {
        return [
            'title.required' => 'Пожалуйста, введите заголовок.',
            'text.required'  => 'Пожалуйста, введите текст.',
            'image.required' => 'Пожалуйста, добавьте изображение.',
        ];
    }

    private function forgetCache()
    {
        Cache::forget('main.page.data');
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsQuery = Post::query()
            ->orderByDesc('id')
            ->where('type', 'news');

        if ($request->get('count'))
        {
            if ($request->get('page'))
            {
                $itemsQuery->offset(($request->get('page') - 1) * $request->get('count'));
            }
            $itemsQuery->take($request->get('count'));
        }

        return $this->success(
            $itemsQuery->get()
                ->toArray(),
            Post::query()
                ->where('type', 'news')
                ->count()
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'type'         => '',
            'description'  => '',
            'source_link'  => '',
            'source_image' => '',
            'title'        => 'required',
            'text'         => 'required',
            'image'        => ['required', 'image']
        ], $this->messages());

        $sourceImagePath = '';
        $imagePath       = $data['image']->store('uploads', 'public');

        if ($data['source_image'] && $data['source_image'] !== 'null')
        {
            $sourceImagePath = $data['source_image']->store('uploads', 'public');
        }

        $post = auth()
            ->user()
            ->posts()
            ->create([
                'title'        => $data['title'],
                'text'         => $data['text'],
                'description'  => $data['description'] ?? '',
                'source_link'  => $data['source_link'] ?? '',
                'source_image' => $sourceImagePath,
                'image'        => $imagePath,
                'type'         => 'news'
            ]);

        $this->forgetCache();

        return $this->success($post->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->success(Post::findOrFail($id)
            ->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);

        $data = $request->validate([
            'active'       => '',
            'type'         => '',
            'description'  => '',
            'source_link'  => '',
            'source_image' => '',
            'title'        => 'required',
            'text'         => 'required',
            'image'        => ''
        ], $this->messages());

        $post->active      = $data['active'] === 'true' || $data['active'] === '1';
        $post->title       = $data['title'];
        $post->text        = $data['text'];
        $post->description = $data['description'] ?? '';
        $post->source_link = $data['source_link'] ?? '';

        if ($data['image'] && $data['image'] !== 'null')
        {
            $post->image = $data['image']->store('uploads', 'public');
        }

        if ($data['source_image'] && $data['source_image'] !== 'null')
        {
            $post->source_image = $data['source_image']->store('uploads', 'public');
        }

        $post->save();

        $this->forgetCache();

        return $this->success($post->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        $this->forgetCache();
        return $this->success(['deleted']);
    }
}
