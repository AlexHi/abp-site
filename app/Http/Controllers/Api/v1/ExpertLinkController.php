<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\ExpertLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ExpertLinkController extends Controller
{

    public function messages()
    {
        return [
            'name.required' => 'Пожалуйста, введите название.',
            'link.required' => 'Пожалуйста, добавьте ссылку.',
            'logo.required' => 'Пожалуйста, добавьте логотип.',
            'logo.image'    => 'Загруженный логотип не является изображением.',
        ];
    }

    private function forgetCache()
    {
        Cache::forget('main.page.data');
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsQuery = ExpertLink::query();

        if ($request->get('count'))
        {
            if ($request->get('page'))
            {
                $itemsQuery->offset(($request->get('page') - 1) * $request->get('count'));
            }
            $itemsQuery->take($request->get('count'));
        }

        return $this->success(
             $itemsQuery->get()
                ->toArray(),
             ExpertLink::query()
                ->count()
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'logo' => ['required', 'image'],
            'link' => 'required',
        ], $this->messages());

        $imagePath = $data['logo']->store('expert-links', 'public');

        $client = ExpertLink::create([
            'name' => $data['name'],
            'link' => $data['link'],
            'logo' => $imagePath
        ]);

        $this->forgetCache();

        return $this->success($client->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\ExpertLink $expertLink
     *
     * @return \Illuminate\Http\Response
     */
    public function show(ExpertLink $expertLink)
    {
        return $this->success($expertLink->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ExpertLink $expertLink
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(ExpertLink $expertLink)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ExpertLink   $expertLink
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExpertLink $expertLink)
    {
        $data = $request->validate([
            'active' => '',
            'name'   => 'required',
            'logo'   => ['required', 'image'],
            'link'   => 'required',
        ], $this->messages());

        $expertLink->active = $data['active'] === 'true' || $data['active'] === '1';
        $expertLink->name   = $data['name'];
        $expertLink->link   = $data['link'];

        if ($data['logo'])
        {
            $expertLink->logo = $data['logo']->store('expert-links', 'public');
        }

        $expertLink->save();

        $this->forgetCache();

        return $this->success($expertLink->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ExpertLink $expertLink
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExpertLink $expertLink)
    {
        $expertLink->delete();
        $this->forgetCache();
        return $this->success(['deleted']);
    }
}
