<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ReviewController extends Controller
{

    public function messages()
    {
        return [
            'name.required'          => 'Пожалуйста, введите название.',
            'work_position.required' => 'Пожалуйста, введите должность.',
            'text.required'          => 'Пожалуйста, введите текст.',
            'logo.required'          => 'Пожалуйста, добавьте логотип.',
            'logo.image'             => 'Загруженный логотип не является изображением',
        ];
    }

    private function forgetCache()
    {
        Cache::forget('main.page.data');
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsQuery = Review::query();

        if ($request->get('count'))
        {
            if ($request->get('page'))
            {
                $itemsQuery->offset(($request->get('page') - 1) * $request->get('count'));
            }
            $itemsQuery->take($request->get('count'));
        }

        return $this->success(
            $itemsQuery->get()
                ->toArray(),
            Review::query()
                ->count()
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'          => 'required',
            'text'          => 'required',
            'logo'          => ['required', 'image'],
            'work_position' => 'required',
        ], $this->messages());

        $imagePath = $data['logo']->store('reviews', 'public');

        $client = Review::create([
            'name'          => $data['name'],
            'text'          => $data['text'],
            'work_position' => $data['work_position'],
            'logo'          => $imagePath
        ]);

        $this->forgetCache();

        return $this->success($client->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Review $review
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        return $this->success($review->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Review $review
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Review       $review
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        $data = $request->validate([
            'active'        => '',
            'work_position' => 'required',
            'name'          => 'required',
            'text'          => 'required',
            'logo'          => ['required', 'image']
        ], $this->messages());

        $review->active        = $data['active'] === 'true' || $data['active'] === '1';
        $review->name          = $data['name'];
        $review->text          = $data['text'];
        $review->work_position = $data['work_position'];

        if ($data['logo'])
        {
            $review->logo = $data['logo']->store('reviews', 'public');
        }

        $review->save();

        $this->forgetCache();

        return $this->success($review->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Review $review
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        $review->delete();
        $this->forgetCache();
        return $this->success(['deleted']);
    }
}
