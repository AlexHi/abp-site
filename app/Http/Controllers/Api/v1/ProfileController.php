<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    public function messages()
    {
        return [
            'description.required'       => 'Пожалуйста, добавьте описание.',
            'education.required'         => 'Пожалуйста, введите образование.',
            'extra_description.required' => 'Пожалуйста, добавьте дополнительное описание.',
            'image.required'             => 'Пожалуйста, добавьте изображение.',
            'languages.required'         => 'Пожалуйста, добавьте владение языками.',
            'phone_number.required'      => 'Пожалуйста, добавьте номер телефона.',
            'specialisation.required'    => 'Пожалуйста, добавьте специализацию.',
            'work_position.required'     => 'Пожалуйста, добавьте должность.',
        ];
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $userId
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $userId)
    {
        $profile = Profile::query()->where('user_id', $userId)->first();

        return $this->success($profile->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        $data = $request->validate([
            'description'       => 'required',
            'education'         => 'required',
            'extra_description' => 'required',
            'image'             => 'required',
            'languages'         => 'required',
            'phone_number'      => 'required',
            'specialisation'    => 'required',
            'work_position'     => 'required',
            'document_id'       => '',
        ], $this->messages());

        $imageOriginalName = $data['image']->getClientOriginalName();

        $imagePath = $data['image']->storeAs('public/profile', $imageOriginalName);

        $profile->description       = $data['description'];
        $profile->education         = $data['education'];
        $profile->extra_description = $data['extra_description'];
        $profile->image             = str_replace('public/', '', $imagePath);
        $profile->languages         = $data['languages'];
        $profile->phone_number      = $data['phone_number'];
        $profile->specialisation    = $data['specialisation'];
        $profile->work_position     = $data['work_position'];
        $profile->document_id       = !$data['document_id'] || $data['document_id'] === 'null' ? 0 : $data['document_id'];

        $profile->save();

        return $this->success($profile->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
