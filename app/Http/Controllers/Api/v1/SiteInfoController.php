<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\SiteInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class SiteInfoController extends Controller
{

    public function messages()
    {
        return [
            'name.required'                   => 'Пожалуйста, введите название.',
            'address.required'                => 'Пожалуйста, введите адрес.',
            'phone_number.required'           => 'Пожалуйста, введите номер телефона.',
            'email.required'                  => 'Пожалуйста, введите email.',
            'work_time.required'              => 'Пожалуйста, введите время работы.',
            'driveway_title.required'         => 'Пожалуйста, введите заголовок для описания проезда.',
            'driveway_description.required'   => 'Пожалуйста, введите описание проезда.',
            'passageway_title.required'       => 'Пожалуйста, введите заголовок для описания пешего пути.',
            'passageway_description.required' => 'Пожалуйста, введите описание пешего пути.',
            'contact_user_id.required'        => 'Пожалуйста, выберите пользователя в контактах.',
            'contact_document_id.required'    => 'Пожалуйста, выберите документ в контактах.',
            'pdpp_document_id.required'       => 'Пожалуйста, выберите файл политики обработки персональных данных.',
            'eula_document_id.required'       => 'Пожалуйста, выберите файл пользовательского соглашения.',
            'header_logo.required'            => 'Пожалуйста, добавьте логотип в заголовке.',
            'footer_logo.required'            => 'Пожалуйста, добавьте логотип в футере.',
            'map_logo.required'               => 'Пожалуйста, добавьте логотип на карте.',
            'passageway_image.required'       => 'Пожалуйста, добавьте изображение пешего пути.',
            'trust_clients_image.required'    => 'Пожалуйста, добавьте изображение клиентов на главной.',
        ];
    }

    private function forgetCache($lang)
    {
        Cache::forget('site_info.' . $lang);
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsQuery = SiteInfo::query();

        if ($request->get('count'))
        {
            if ($request->get('page'))
            {
                $itemsQuery->offset(($request->get('page') - 1) * $request->get('count'));
            }
            $itemsQuery->take($request->get('count'));
        }

        return $this->success(
            $itemsQuery->get()
                ->toArray(),
            SiteInfo::query()
                ->count()
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->success(SiteInfo::findOrFail($id)
            ->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\SiteInfo $siteInfo
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteInfo $siteInfo)
    {
        $this->forgetCache($siteInfo->lang);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\SiteInfo     $siteInfo
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteInfo $siteInfo)
    {
        $data = $request->validate([
            'name'                   => 'required',
            'address'                => 'required',
            'phone_number'           => 'required',
            'email'                  => 'required',
            'work_time'              => 'required',
            'map_latitude'           => '',
            'map_longitude'          => '',
            'driveway_title'         => 'required',
            'driveway_description'   => 'required',
            'passageway_title'       => 'required',
            'passageway_description' => 'required',
            'contact_user_id'        => 'required',
            'contact_document_id'    => 'required',
            'pdpp_document_id'       => 'required',
            'eula_document_id'       => 'required',
            'header_logo'            => ['required'],
            'footer_logo'            => ['required'],
            'map_logo'               => ['required'],
            'passageway_image'       => ['required'],
            'trust_clients_image'    => ['required'],
            'script_head'            => '',
            'script_before_body'     => '',
            'script_after_body'      => '',
            'meta_description'       => '',
            'meta_keywords'          => '',
        ], $this->messages());

        $siteInfo->name                   = $data['name'];
        $siteInfo->address                = $data['address'];
        $siteInfo->phone_number           = $data['phone_number'];
        $siteInfo->email                  = $data['email'];
        $siteInfo->work_time              = $data['work_time'];
        $siteInfo->map_latitude           = $data['map_latitude'];
        $siteInfo->map_longitude          = $data['map_longitude'];
        $siteInfo->driveway_title         = $data['driveway_title'];
        $siteInfo->driveway_description   = $data['driveway_description'];
        $siteInfo->passageway_title       = $data['passageway_title'];
        $siteInfo->passageway_description = $data['passageway_description'];
        $siteInfo->contact_user_id        = $data['contact_user_id'];
        $siteInfo->contact_document_id    = $data['contact_document_id'];
        $siteInfo->pdpp_document_id       = $data['pdpp_document_id'];
        $siteInfo->eula_document_id       = $data['eula_document_id'];
        $siteInfo->script_head            = $data['script_head'] ?? '';
        $siteInfo->script_before_body     = $data['script_before_body'] ?? '';
        $siteInfo->script_after_body      = $data['script_after_body'] ?? '';
        $siteInfo->meta_description       = $data['meta_description'] ?? '';
        $siteInfo->meta_keywords          = $data['meta_keywords'] ?? '';

        foreach (['header_logo', 'footer_logo', 'map_logo', 'passageway_image', 'trust_clients_image'] as $key)
        {
            $siteInfo[$key] = $data[$key]->store('site_info', 'public');
        }

        $siteInfo->save();

        $this->forgetCache($siteInfo->lang);

        return $this->success($siteInfo->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\SiteInfo $siteInfo
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteInfo $siteInfo)
    {
        $this->forgetCache($siteInfo->lang);
    }
}
