<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Service;
use App\Models\ServicesCategory;
use Illuminate\Http\Request;

class ServicesCategoryController extends Controller
{

    public function messages()
    {
        return [
            'name.required' => 'Пожалуйста, введите название.',
            'sort.required' => 'Пожалуйста, введите сортировку.',
            'icon.required' => 'Пожалуйста, добавьте иконку.',
            'icon.image'    => 'Загруженная иконка не является изображением',
        ];
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->success(ServicesCategory::query()->orderByDesc('sort')->get()->toArray());
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'sort' => 'required',
            'icon' => ['required', 'image']
        ], $this->messages());

        $imagePath = $data['icon']->store('services', 'public');

        $servicesCategory = new ServicesCategory();

        $servicesCategory->name = $data['name'];
        $servicesCategory->sort = $data['sort'];
        $servicesCategory->icon = $imagePath;

        $servicesCategory->save();

        return $this->success($servicesCategory->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\ServicesCategory $servicesCategory
     *
     * @return \Illuminate\Http\Response
     */
    public function show(ServicesCategory $servicesCategory)
    {
        return $this->success($servicesCategory->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ServicesCategory $servicesCategory
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(ServicesCategory $servicesCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request     $request
     * @param \App\Models\ServicesCategory $servicesCategory
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServicesCategory $servicesCategory)
    {
        $data = $request->validate([
            'active' => '',
            'name'   => 'required',
            'sort'   => 'required',
            'icon'   => ''
        ], $this->messages());

        if ($data['icon'])
        {
            $imagePath = $data['icon']->store('services', 'public');
            if ($imagePath)
            {
                $servicesCategory->icon = $imagePath;
            }
        }

        $servicesCategory->active = $data['active'] === 'true' || $data['active'] === '1';
        $servicesCategory->name   = $data['name'];
        $servicesCategory->sort   = $data['sort'];

        $servicesCategory->save();

        return $this->success($servicesCategory->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ServicesCategory $servicesCategory
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServicesCategory $servicesCategory)
    {
        $servicesCategory->delete();
        return $this->success(['deleted']);
    }
}
