<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{

    public function messages()
    {
        return [
            'title.required'                => 'Пожалуйста, введите название.',
            'description.required'          => 'Пожалуйста, введите описание.',
            'text.required'                 => 'Пожалуйста, введите текст.',
            'client_id.required'            => 'Пожалуйста, выберите клиента.',
            'user_id.required'              => 'Пожалуйста, выберите ответственного.',
            'projects_category_id.required' => 'Пожалуйста, выберите категорию.',
        ];
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $projectsQuery = Project::query();

        if ($request->get('category_id'))
        {
            $projectsQuery->where('projects_category_id', $request->get('category_id'));
        }

        $projects = $projectsQuery->get();
        return $this->success($projects->toArray());
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title'                => 'required',
            'description'          => 'required',
            'text'                 => 'required',
            'client_id'            => 'required',
            'user_id'              => 'required',
            'projects_category_id' => 'required'
        ], $this->messages());

        $project = new Project();

        $project->title                = $data['title'];
        $project->description          = $data['description'];
        $project->text                 = $data['text'];
        $project->client_id            = $data['client_id'];
        $project->user_id              = $data['user_id'];
        $project->projects_category_id = $data['projects_category_id'];

        $project->save();

        return $this->success($project->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Project $project
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return $this->success($project->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Project $project
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project      $project
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $data = $request->validate([
            'active'               => '',
            'title'                => 'required',
            'description'          => 'required',
            'text'                 => 'required',
            'client_id'            => 'required',
            'user_id'              => 'required',
            'projects_category_id' => 'required'
        ], $this->messages());

        $project->active               = $data['active'] === 'true' || $data['active'] === '1';
        $project->title                = $data['title'];
        $project->description          = $data['description'];
        $project->text                 = $data['text'];
        $project->client_id            = $data['client_id'];
        $project->user_id              = $data['user_id'];
        $project->projects_category_id = $data['projects_category_id'];

        $project->save();

        return $this->success($project->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Project $project
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return $this->success(['deleted']);
    }
}
