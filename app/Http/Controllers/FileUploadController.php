<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileUploadController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param string  $bundle
     *
     * @return \Illuminate\Http\Response
     */
    public function fileUpload(Request $request, string $bundle)
    {
        $request->validate([
            'file' => 'required',
        ]);

        $filePath = $request->file->store('uploads', 'public');

        if (!$filePath)
        {
            return $this->error('Не удалось загрузить файл');
        }

        $filePath = '/storage/' . $filePath;

        return $this->success(['file_path' => $filePath]);
    }
}
