<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Project;
use App\Models\ProjectsCategory;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{

    public function index()
    {
        $projectCategories = ProjectsCategory::query()->where('active', true)->with([
            'projects' => function ($query)
            {
                $query->where('active', true)->with(['user', 'client']);
            }
        ])->orderByDesc('sort')->get();

        setlocale(LC_TIME, 'ru_RU.UTF-8');

        return view('public.projects.index', ['project_categories' => $projectCategories]);
    }

    public function show(int $id)
    {
        $project = Project::query()->whereKey($id)->with(['user', 'client'])->firstOrFail();

        return view('public.projects.show', ['project' => $project]);
    }
}
