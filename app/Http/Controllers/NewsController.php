<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class NewsController extends Controller
{

    public function index()
    {
        $posts = Post::query()->where('active', true)->where('type', 'news')->latest()->paginate(10);

        setlocale(LC_TIME, 'ru_RU.UTF-8');

        return view('public.news.index', [
            'posts' => $posts
        ]);
    }

    public function show(Post $post)
    {
        setlocale(LC_TIME, 'ru_RU.UTF-8');

        $posts = Post::query()->where('active', true)->where('type', 'news')->latest()->take(4)->get();

        if ($post->type !== 'news') {
            abort(404);
        }

        return view('public.news.show', [
            'post'  => $post,
            'posts' => $posts
        ]);
    }
}
