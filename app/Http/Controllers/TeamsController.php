<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class TeamsController extends Controller
{

    public function index()
    {
        $users = User::query()->where('show_in_teams', true)->get();

        setlocale(LC_TIME, 'ru_RU.UTF-8');

        return view('public.teams.index', [
            'users' => $users
        ]);
    }

    public function show(int $id)
    {

        $user = User::query()->whereKey($id)->with([
            'profile',
            'posts'    => function ($query)
            {
                $query->latest()
                      ->take(10);
            },
            'projects' => function ($query)
            {
                $query->latest()
                      ->take(10)
                      ->with('client');
            }
        ])->firstOrFail();

        setlocale(LC_TIME, 'ru_RU.UTF-8');

        return view('public.teams.show', ['teamUser' => $user]);
    }
}
