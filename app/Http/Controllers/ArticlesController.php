<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{

    public function index()
    {
        $posts = Post::query()->where('active', true)->where('type', 'articles')->latest()->paginate(10);

        setlocale(LC_TIME, 'ru_RU.UTF-8');

        return view('public.articles.index', [
            'posts' => $posts
        ]);
    }

    public function show(Post $post)
    {
        setlocale(LC_TIME, 'ru_RU.UTF-8');

        $posts = Post::query()->where('active', true)->where('type', 'articles')->latest()->take(4)->get();

        if ($post->type !== 'articles') {
            abort(404);
        }

        return view('public.articles.show', [
            'post'  => $post,
            'posts' => $posts
        ]);
    }
}
