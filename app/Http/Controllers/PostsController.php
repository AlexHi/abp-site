<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('admin.posts.create');
    }

    public function store()
    {
        $data = \request()->validate([
            'title' => 'required',
            'text'  => 'required',
            'image' => ['required', 'image']
        ]);

        $imagePath = \request('image')->store('uploads', 'public');

        auth()->user()->posts()->create([
            'title' => $data['title'],
            'text'  => $data['text'],
            'image' => $imagePath
        ]);

        return redirect()->back();
    }
}
