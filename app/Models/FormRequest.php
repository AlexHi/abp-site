<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FormRequest
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $phone_number
 * @property string|null $subject
 * @property string $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $form_name
 * @property string $thing_url
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest whereFormName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest whereThingUrl($value)
 * @property string|null $thing_name
 * @method static \Illuminate\Database\Eloquent\Builder|FormRequest whereThingName($value)
 */
class FormRequest extends Model
{
    use HasFactory;

    protected $dates = [
        'created_at' .
        'updated_at'
    ];

    public function toArray()
    {
        $array = parent::toArray();

        $array['created_at'] = $this->created_at ? $this->created_at->format('d.m.Y H:i:s') : null;
        $array['updated_at'] = $this->created_at ? $this->updated_at->format('d.m.Y H:i:s') : null;

        return $array;
    }
}
