<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * App\Models\Video
 *
 * @property int                             $id
 * @property string                          $name
 * @property string                          $link
 * @property string                          $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Video newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Video newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Video query()
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null                     $youtube_code
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereYoutubeCode($value)
 * @property int                             $active
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereActive($value)
 */
class Video extends Model implements Searchable
{

    use HasFactory;

    protected $fillable = [
        'active',
        'name',
        'link',
        'description',
        'youtube_code'
    ];

    protected $dates = [
        'created_at' .
        'updated_at'
    ];

    public function toArray()
    {
        $array = parent::toArray();

        $array['created_at'] = $this->created_at ? $this->created_at->format('d.m.Y H:i:s') : null;
        $array['updated_at'] = $this->created_at ? $this->updated_at->format('d.m.Y H:i:s') : null;

        return $array;
    }

    public function getSearchResult() : SearchResult
    {
        $url = route('public.videos.show', $this->id);

        return new SearchResult(
            $this,
            $this->name,
            $url
        );
    }
}
