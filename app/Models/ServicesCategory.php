<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ServicesCategory
 *
 * @property int $id
 * @property string|null $icon
 * @property string $name
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Service[] $services
 * @property-read int|null $services_count
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesCategory whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesCategory whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $active
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesCategory whereActive($value)
 */
class ServicesCategory extends Model
{
    use HasFactory;

    protected $dates = [
        'created_at' .
        'updated_at'
    ];

    public function toArray()
    {
        $array = parent::toArray();

        $array['created_at'] = $this->created_at ? $this->created_at->format('d.m.Y H:i:s') : null;
        $array['updated_at'] = $this->created_at ? $this->updated_at->format('d.m.Y H:i:s') : null;

        return $array;
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }
}
