<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * App\Models\Project
 *
 * @property int                               $id
 * @property string                            $title
 * @property int|null                          $client_id
 * @property int|null                          $user_id
 * @property int                               $projects_category_id
 * @property string                            $description
 * @property string                            $text
 * @property \Illuminate\Support\Carbon|null   $created_at
 * @property \Illuminate\Support\Carbon|null   $updated_at
 * @property-read \App\Models\ProjectsCategory $category
 * @property-read \App\Models\Client|null      $client
 * @property-read \App\Models\User|null        $user
 * @method static \Illuminate\Database\Eloquent\Builder|Project newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Project newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Project query()
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereProjectsCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereUserId($value)
 * @mixin \Eloquent
 * @property int                               $active
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereActive($value)
 */
class Project extends Model implements Searchable
{

    use HasFactory;

    protected $dates = [
        'created_at' .
        'updated_at'
    ];

    protected $casts = [
        'client_id'            => 'integer',
        'user_id'              => 'integer',
        'projects_category_id' => 'integer'
    ];

    public function toArray()
    {
        $array = parent::toArray();

        $array['created_at'] = $this->created_at ? $this->created_at->format('d.m.Y H:i:s') : null;
        $array['updated_at'] = $this->created_at ? $this->updated_at->format('d.m.Y H:i:s') : null;

        return $array;
    }

    public function category()
    {
        return $this->belongsTo(ProjectsCategory::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getSearchResult() : SearchResult
    {
        $url = route('public.projects.show', $this->id);

        return new SearchResult(
            $this,
            $this->title,
            $url
        );
    }
}
