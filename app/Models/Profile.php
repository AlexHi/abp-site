<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Profile
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $phone_number
 * @property string|null $work_position
 * @property string|null $description
 * @property string|null $education
 * @property string|null $languages
 * @property string|null $specialisation
 * @property string|null $extra_description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $image
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Profile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Profile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Profile query()
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereEducation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereExtraDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereLanguages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereSpecialisation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereWorkPosition($value)
 * @mixin \Eloquent
 * @property int|null $document_id
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereDocumentId($value)
 * @property-read \App\Models\Document|null $document
 */
class Profile extends Model
{

    use HasFactory;

    protected $fillable = [
        'phone_number',
        'work_position',
        'description',
        'education',
        'languages',
        'specialisation',
        'extra_description',
        'image',
        'user_id'
    ];

    protected $dates = [
        'created_at' .
        'updated_at'
    ];

    public function toArray()
    {
        $array = parent::toArray();

        $array['created_at'] = $this->created_at ? $this->created_at->format('d.m.Y H:i:s') : null;
        $array['updated_at'] = $this->created_at ? $this->updated_at->format('d.m.Y H:i:s') : null;

        return $array;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function document()
    {
        return $this->hasOne(Document::class, 'id', 'document_id');
    }
}
