<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SocialMedia
 *
 * @property int                             $id
 * @property int                             $site_info_id
 * @property string                          $logo
 * @property string                          $name
 * @property string                          $link
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\SiteInfo       $siteInfo
 * @method static \Illuminate\Database\Eloquent\Builder|SocialMedia newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SocialMedia newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SocialMedia query()
 * @method static \Illuminate\Database\Eloquent\Builder|SocialMedia whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocialMedia whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocialMedia whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocialMedia whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocialMedia whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocialMedia whereSiteInfoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocialMedia whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $active
 * @method static \Illuminate\Database\Eloquent\Builder|SocialMedia whereActive($value)
 */
class SocialMedia extends Model
{

    use HasFactory;

    protected $fillable = [
        'active',
        'name',
        'link',
        'logo',
        'site_info_id'
    ];

    protected $dates = [
        'created_at' .
        'updated_at'
    ];

    public function toArray()
    {
        $array = parent::toArray();

        $array['created_at'] = $this->created_at ? $this->created_at->format('d.m.Y H:i:s') : null;
        $array['updated_at'] = $this->created_at ? $this->updated_at->format('d.m.Y H:i:s') : null;

        return $array;
    }

    public function siteInfo()
    {
        return $this->belongsTo(SiteInfo::class);
    }
}
