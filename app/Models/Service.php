<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * App\Models\Service
 *
 * @property int                               $id
 * @property string                            $name
 * @property string                            $minimal_price
 * @property string                            $text
 * @property string|null                       $total_price
 * @property string|null                       $price_note
 * @property mixed|null                        $sub_services
 * @property mixed|null                        $steps
 * @property int                               $services_category_id
 * @property \Illuminate\Support\Carbon|null   $created_at
 * @property \Illuminate\Support\Carbon|null   $updated_at
 * @property-read \App\Models\ServicesCategory $category
 * @method static \Illuminate\Database\Eloquent\Builder|Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Service query()
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereMinimalPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service wherePriceNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereServicesCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereSteps($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereSubServices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereTotalPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int                               $active
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereActive($value)
 */
class Service extends Model implements Searchable
{

    use HasFactory;

    protected $dates = [
        'created_at' .
        'updated_at'
    ];

    public function toArray()
    {
        $array = parent::toArray();

        $array['created_at'] = $this->created_at ? $this->created_at->format('d.m.Y H:i:s') : null;
        $array['updated_at'] = $this->created_at ? $this->updated_at->format('d.m.Y H:i:s') : null;

        return $array;
    }

    public function category()
    {
        return $this->belongsTo(ServicesCategory::class);
    }

    public function getSearchResult() : SearchResult
    {
        $url = route('public.services.show', $this->id);

        return new SearchResult(
            $this,
            $this->name,
            $url
        );
    }
}
