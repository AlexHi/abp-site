<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProjectsCategory
 *
 * @property int $id
 * @property string|null $icon
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $projects
 * @property-read int|null $projects_count
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsCategory whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $sort
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsCategory whereSort($value)
 * @property int $active
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsCategory whereActive($value)
 */
class ProjectsCategory extends Model
{
    use HasFactory;

    protected $fillable = [

    ];

    protected $dates = [
        'created_at' .
        'updated_at'
    ];

    public function toArray()
    {
        $array = parent::toArray();

        $array['created_at'] = $this->created_at ? $this->created_at->format('d.m.Y H:i:s') : null;
        $array['updated_at'] = $this->created_at ? $this->updated_at->format('d.m.Y H:i:s') : null;

        return $array;
    }


    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
