<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ExpertLink
 *
 * @property int $id
 * @property string $name
 * @property string $link
 * @property string $logo
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLink newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLink newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLink query()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLink whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLink whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLink whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLink whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLink whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLink whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $active
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLink whereActive($value)
 */
class ExpertLink extends Model
{

    use HasFactory;

    protected $fillable = [
        'active',
        'name',
        'link',
        'logo'
    ];

    protected $dates = [
        'created_at' .
        'updated_at'
    ];

    public function toArray()
    {
        $array = parent::toArray();

        $array['created_at'] = $this->created_at ? $this->created_at->format('d.m.Y H:i:s') : null;
        $array['updated_at'] = $this->created_at ? $this->updated_at->format('d.m.Y H:i:s') : null;

        return $array;
    }
}
