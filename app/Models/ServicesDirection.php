<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ServicesDirection
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesDirection newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesDirection newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesDirection query()
 * @mixin \Eloquent
 * @property int                             $id
 * @property string                          $name
 * @property string                          $icon
 * @property mixed                           $services_list
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesDirection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesDirection whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesDirection whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesDirection whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesDirection whereServicesList($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesDirection whereUpdatedAt($value)
 * @property int                             $sort
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesDirection whereSort($value)
 * @property int                             $active
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesDirection whereActive($value)
 * @property int|null                        $services_category_id
 * @property string|null                     $link
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesDirection whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicesDirection whereServicesCategoryId($value)
 * @property-read \App\Models\ServicesCategory|null $category
 */
class ServicesDirection extends Model
{

    use HasFactory;

    protected $dates = [
        'created_at' .
        'updated_at'
    ];

    public function toArray()
    {
        $array = parent::toArray();

        $array['created_at'] = $this->created_at ? $this->created_at->format('d.m.Y H:i:s') : null;
        $array['updated_at'] = $this->created_at ? $this->updated_at->format('d.m.Y H:i:s') : null;

        return $array;
    }

    public function category()
    {
        return $this->belongsTo(ServicesCategory::class,  'services_category_id', 'id');
    }
}
