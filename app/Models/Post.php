<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use Tightenco\Collect\Contracts\Support\Arrayable;

/**
 * App\Models\Post
 *
 * @property int                             $id
 * @property int                             $user_id
 * @property string                          $title
 * @property string                          $image
 * @property string|null                     $description
 * @property string                          $text
 * @property string|null                     $source_link
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null                     $type
 * @property-read \App\Models\User           $user
 * @method static \Illuminate\Database\Eloquent\Builder|Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereSourceLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUserId($value)
 * @mixin \Eloquent
 * @property string|null                     $source_image
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereSourceImage($value)
 * @property int                             $active
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereActive($value)
 */
class Post extends Model implements Searchable
{

    use HasFactory;

    protected $fillable = [
        'active',
        'title',
        'image',
        'description',
        'text',
        'type',
        'source_link',
        'user_id'
    ];

    protected $dates = [
        'created_at' .
        'updated_at'
    ];

    public function toArray()
    {
        $array = parent::toArray();

        $array['created_at'] = $this->created_at ? $this->created_at->format('d.m.Y H:i:s') : null;
        $array['updated_at'] = $this->created_at ? $this->updated_at->format('d.m.Y H:i:s') : null;

        return $array;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getSearchResult() : SearchResult
    {
        $url = route($this->type === 'article' ? 'public.articles.show' : 'public.news.show', $this->id);

        return new SearchResult(
            $this,
            $this->title,
            $url
        );
    }
}
