<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SiteInfo
 *
 * @property int                                                                     $id
 * @property string                                                                  $lang
 * @property string|null                                                             $header_logo
 * @property string|null                                                             $footer_logo
 * @property string|null                                                             $address
 * @property string|null                                                             $phone_number
 * @property string|null                                                             $email
 * @property string|null                                                             $work_time
 * @property string|null                                                             $map_logo
 * @property string|null                                                             $map_latitude
 * @property string|null                                                             $map_longitude
 * @property string|null                                                             $driveway_title
 * @property string|null                                                             $driveway_description
 * @property string|null                                                             $passageway_image
 * @property string|null                                                             $passageway_title
 * @property string|null                                                             $passageway_description
 * @property string|null                                                             $trust_clients_image
 * @property string|null                                                             $main_page_first_block
 * @property \Illuminate\Support\Carbon|null                                         $created_at
 * @property \Illuminate\Support\Carbon|null                                         $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SocialMedia[] $socialMedias
 * @property-read int|null                                                           $social_medias_count
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo query()
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereDrivewayDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereDrivewayTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereFooterLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereHeaderLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereMainPageFirstBlock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereMapLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereMapLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereMapLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo wherePassagewayDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo wherePassagewayImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo wherePassagewayTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereTrustClientsImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereWorkTime($value)
 * @mixin \Eloquent
 * @property string                                                                  $name
 * @property int|null                                                                $contact_user_id
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereContactUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereName($value)
 * @property int|null                                                                $contact_document_id
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereContactDocumentId($value)
 * @property int|null                                                                $pdpp_document_id
 * @property int|null                                                                $eula_document_id
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereEulaDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo wherePdppDocumentId($value)
 * @property-read \App\Models\Document|null $contactDocument
 * @property-read \App\Models\Document|null $eulaDocument
 * @property-read \App\Models\Document|null $pdppDocument
 * @property string|null $script_head
 * @property string|null $script_before_body
 * @property string|null $script_after_body
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereMetaKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereScriptAfterBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereScriptBeforeBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteInfo whereScriptHead($value)
 */
class SiteInfo extends Model
{

    use HasFactory;

    protected $fillable = [
        'lang',
        'header_logo',
        'footer_logo',
        'address',
        'phone_number',
        'email',
        'map_logo',
        'map_latitude',
        'map_longitude',
        'driveway_title',
        'driveway_description',
        'passageway_image',
        'passageway_title',
        'passageway_description',
        'trust_clients_image',
        'main_page_first_block',
    ];

    protected $dates = [
        'created_at' .
        'updated_at'
    ];

    public function toArray()
    {
        $array = parent::toArray();

        $array['created_at'] = $this->created_at ? $this->created_at->format('d.m.Y H:i:s') : null;
        $array['updated_at'] = $this->updated_at ? $this->updated_at->format('d.m.Y H:i:s') : null;

        return $array;
    }

    public function socialMedias()
    {
        return $this->hasMany(SocialMedia::class);
    }

    public function contactDocument()
    {
        return $this->hasOne(Document::class, 'id', 'contact_document_id');
    }

    public function pdppDocument()
    {
        return $this->hasOne(Document::class, 'id', 'pdpp_document_id');
    }

    public function eulaDocument()
    {
        return $this->hasOne(Document::class, 'id', 'eula_document_id');
    }
}
