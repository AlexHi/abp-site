<?php

namespace App\Providers;

use App\Models\SiteInfo;
use App\Models\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     * @return void
     */
    public function boot()
    {
        View::composer(['layouts.app', 'admin.index', 'admin.auth', 'public.*'], function ($view)
        {
            $siteInfo = Cache::remember('site_info.ru', 3600, function ()
            {
                return SiteInfo::query()->where('lang', 'ru')
                               ->with([
                                   'socialMedias' => function ($query)
                                   {
                                       $query->where('active', true);
                                   },
                                   'contactDocument',
                                   'pdppDocument',
                                   'eulaDocument'
                               ])
                               ->first();
            });

            $view->with('site_info', $siteInfo);
            $view->with('user', auth()->user());
            if (auth()->user())
            {
                $view->with('user', User::query()->whereKey(auth()->user()->id)->with('profile')->first());
            }
        });
    }
}
