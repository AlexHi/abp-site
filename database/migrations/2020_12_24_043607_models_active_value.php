<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModelsActiveValue extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table)
        {
            $table->boolean('active')->default(true);
        });
        Schema::table('expert_links', function (Blueprint $table)
        {
            $table->boolean('active')->default(true);
        });
        Schema::table('pages', function (Blueprint $table)
        {
            $table->boolean('active')->default(true);
        });
        Schema::table('posts', function (Blueprint $table)
        {
            $table->boolean('active')->default(true);
        });
        Schema::table('projects', function (Blueprint $table)
        {
            $table->boolean('active')->default(true);
        });
        Schema::table('projects_categories', function (Blueprint $table)
        {
            $table->boolean('active')->default(true);
        });
        Schema::table('reviews', function (Blueprint $table)
        {
            $table->boolean('active')->default(true);
        });
        Schema::table('services', function (Blueprint $table)
        {
            $table->boolean('active')->default(true);
        });
        Schema::table('services_categories', function (Blueprint $table)
        {
            $table->boolean('active')->default(true);
        });
        Schema::table('services_directions', function (Blueprint $table)
        {
            $table->boolean('active')->default(true);
        });
        Schema::table('social_media', function (Blueprint $table)
        {
            $table->boolean('active')->default(true);
        });
        Schema::table('videos', function (Blueprint $table)
        {
            $table->boolean('active')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table)
        {
            $table->dropColumn('active');
        });
        Schema::table('expert_links', function (Blueprint $table)
        {
            $table->dropColumn('active');
        });
        Schema::table('pages', function (Blueprint $table)
        {
            $table->dropColumn('active');
        });
        Schema::table('posts', function (Blueprint $table)
        {
            $table->dropColumn('active');
        });
        Schema::table('projects', function (Blueprint $table)
        {
            $table->dropColumn('active');
        });
        Schema::table('projects_categories', function (Blueprint $table)
        {
            $table->dropColumn('active');
        });
        Schema::table('reviews', function (Blueprint $table)
        {
            $table->dropColumn('active');
        });
        Schema::table('services', function (Blueprint $table)
        {
            $table->dropColumn('active');
        });
        Schema::table('services_categories', function (Blueprint $table)
        {
            $table->dropColumn('active');
        });
        Schema::table('services_directions', function (Blueprint $table)
        {
            $table->dropColumn('active');
        });
        Schema::table('social_media', function (Blueprint $table)
        {
            $table->dropColumn('active');
        });
        Schema::table('videos', function (Blueprint $table)
        {
            $table->dropColumn('active');
        });
    }
}
