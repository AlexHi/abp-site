<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteInfosTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('site_infos', function (Blueprint $table)
        {
            $table->id();
            $table->string('name')->nullable();
            $table->string('lang')->default('ru');
            $table->string('header_logo')->nullable();
            $table->string('footer_logo')->nullable();
            $table->text('address')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('email')->nullable();
            $table->string('work_time')->nullable();
            $table->string('map_logo')->nullable();
            $table->string('map_latitude')->nullable();
            $table->string('map_longitude')->nullable();
            $table->string('driveway_title')->nullable();
            $table->text('driveway_description')->nullable();
            $table->string('passageway_image')->nullable();
            $table->string('passageway_title')->nullable();
            $table->text('passageway_description')->nullable();
            $table->string('trust_clients_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_infos');
    }
}
