<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ServiceDirectionLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services_directions', function (Blueprint $table)
        {
            $table->integer('services_category_id')->nullable();
            $table->string('link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services_directions', function (Blueprint $table)
        {
            $table->dropColumn('services_category_id');
            $table->dropColumn('link');
        });
    }
}
