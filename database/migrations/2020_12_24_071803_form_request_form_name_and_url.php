<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FormRequestFormNameAndUrl extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('form_requests', function (Blueprint $table)
        {
            $table->string('form_name')->nullable();
            $table->string('thing_name')->nullable();
            $table->string('thing_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('form_requests', function (Blueprint $table)
        {
            $table->dropColumn('form_name');
            $table->dropColumn('thing_name');
            $table->dropColumn('thing_url');
        });
    }
}
