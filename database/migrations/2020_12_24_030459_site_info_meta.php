<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SiteInfoMeta extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('site_infos', function (Blueprint $table)
        {
            $table->text('script_head');
            $table->text('script_before_body');
            $table->text('script_after_body');
            $table->text('meta_description');
            $table->text('meta_keywords');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('site_infos', function (Blueprint $table)
        {
            $table->dropColumn('script_head');
            $table->dropColumn('script_before_body');
            $table->dropColumn('script_after_body');
            $table->dropColumn('meta_description');
            $table->dropColumn('meta_keywords');
        });
    }
}
