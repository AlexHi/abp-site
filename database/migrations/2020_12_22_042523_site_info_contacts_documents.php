<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SiteInfoContactsDocuments extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('site_infos', function (Blueprint $table)
        {
            $table->integer('contact_document_id')->nullable();
            $table->integer('pdpp_document_id')->nullable();
            $table->integer('eula_document_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('site_infos', function (Blueprint $table)
        {
            $table->dropColumn('contact_document_id');
            $table->dropColumn('pdpp_document_id');
            $table->dropColumn('eula_document_id');
        });
    }
}
