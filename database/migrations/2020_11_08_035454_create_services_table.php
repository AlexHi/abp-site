<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table)
        {
            $table->id();
            $table->string('name');
            $table->string('minimal_price');
            $table->longText('text');
            $table->string('total_price')->nullable();
            $table->string('price_note')->nullable();
            $table->json('sub_services')->nullable();
            $table->json('steps')->nullable();
            $table->integer('services_category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
