<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesDirectionsTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('services_directions', function (Blueprint $table)
        {
            $table->id();
            $table->string('name');
            $table->string('icon');
            $table->json('services_list');
            $table->integer('sort')->default('100');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_directions');
    }
}
