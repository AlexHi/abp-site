<?php

namespace Database\Seeders;

use App\Models\SiteInfo;
use Illuminate\Database\Seeder;

class SiteInfoSeeder extends Seeder
{

    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        SiteInfo::create([
            'name'                   => 'Основной',
            'lang'                   => 'ru',
            'header_logo'            => 'site_info/default_header_logo.svg',
            'footer_logo'            => 'site_info/default_footer_logo.svg',
            'address'                => 'м. Добрынинская, г. Москва, 1-й Щипковский пер., 1',
            'phone_number'           => '+7 (499) 990-72-35',
            'email'                  => 'abp@abp.legal',
            'work_time'              => 'Пн–Пт с 10:00 до 20:00',
            'map_logo'               => 'site_info/default_map_logo.png',
            'map_latitude'           => '55.721833',
            'map_longitude'          => '37.628882',
            'driveway_title'         => 'На автомобиле с садового кольца (Крымский Вал, ул. Валовая)',
            'driveway_description'   => 'Выходите через станцию Серпуховскую, выход 2. Выйдя из метро нужно обойти павильон и идти на юг (в сторону от центра) по ул. Большая Серпуховская (далее ул. Павловская) около 700 метров до сквера. Не переходя дорогу первое здание слева - бывший офис «MTV Россия». Вход со стороны сквера, второй подъезд. Отдельный лифт на наш 4-ый этаж',
            'passageway_image'       => 'site_info/default_passageway_image.png',
            'passageway_title'       => '«Пешком от метро «Серпуховская/ Добрынинская»',
            'passageway_description' => 'Выходите через станцию Серпуховскую, выход 2. Выйдя из метро нужно обойти павильон и идти на юг (в сторону от центра) по ул. Большая Серпуховская (далее ул. Павловская) около 700 метров до сквера. Не переходя дорогу первое здание слева - бывший офис «MTV Россия». Вход со стороны сквера, второй подъезд. Отдельный лифт на наш 4-ый этаж',
            'trust_clients_image'    => 'site_info/default_trust_clients_image.jpg',
            'script_head'            => '',
            'script_before_body'     => '',
            'script_after_body'      => '',
            'meta_description'       => '',
            'meta_keywords'          => '',
        ]);
    }
}
