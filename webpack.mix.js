const mix = require('laravel-mix');
require('laravel-mix-bundle-analyzer');
require('vuetifyjs-mix-extension');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
/*mix.extend('vuetify', new class {
    webpackConfig(config) {
        config.plugins.push(new VuetifyLoaderPlugin());
    }
});
mix.vuetify();*/

mix.js('resources/js/app.js', 'public/js').vuetify('vuetify-loader', {
    filename: 'css/vuetify-components.css'
});

mix.sass('resources/sass/style.scss', 'public/css');

mix.options({
    cssNano: {
        preset: [
            'advanced', {
                discardComments: {
                    removeAll: true
                }
            }
        ]
    }
});


if (process.env.npm_lifecycle_event !== 'hot') {
    mix.minify('public/js/app.js').version();
    mix.minify('public/css/style.css').version();
}

if (!mix.inProduction()) {
    mix.bundleAnalyzer();
}
