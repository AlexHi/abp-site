import VueRouter  from 'vue-router';
import Vue        from 'vue';
import AdminLogin from './pages/admin/admin-login';
import AdminIndex from './pages/admin/index';

import AdminTest from './pages/admin/test';

import AdminNewsIndex  from './pages/admin/news/index';
import AdminNewsCreate from './pages/admin/news/create';
import AdminNewsEdit   from './pages/admin/news/edit';

import AdminArticlesIndex  from './pages/admin/articles/index';
import AdminArticlesCreate from './pages/admin/articles/create';
import AdminArticlesEdit   from './pages/admin/articles/edit';

import AdminUsersIndex  from './pages/admin/users/index';
import AdminUsersCreate from './pages/admin/users/create';
import AdminUsersEdit   from './pages/admin/users/edit';

import AdminClientsIndex  from './pages/admin/clients/index';
import AdminClientsCreate from './pages/admin/clients/create';
import AdminClientsEdit   from './pages/admin/clients/edit';

import AdminDocumentsIndex  from './pages/admin/documents/index';
import AdminDocumentsCreate from './pages/admin/documents/create';
import AdminDocumentsEdit   from './pages/admin/documents/edit';

import AdminProjectsCategoriesIndex  from './pages/admin/projects/categories/index';
import AdminProjectsCategoriesCreate from './pages/admin/projects/categories/create';
import AdminProjectsCategoriesEdit   from './pages/admin/projects/categories/edit';

import AdminProjectsIndex  from './pages/admin/projects/index';
import AdminProjectsCreate from './pages/admin/projects/create';
import AdminProjectsEdit   from './pages/admin/projects/edit';

import AdminServicesCategoriesIndex  from './pages/admin/services/categories/index';
import AdminServicesCategoriesCreate from './pages/admin/services/categories/create';
import AdminServicesCategoriesEdit   from './pages/admin/services/categories/edit';

import AdminServicesIndex  from './pages/admin/services/index';
import AdminServicesCreate from './pages/admin/services/create';
import AdminServicesEdit   from './pages/admin/services/edit';

import AdminVideosIndex  from './pages/admin/videos/index';
import AdminVideosCreate from './pages/admin/videos/create';
import AdminVideosEdit   from './pages/admin/videos/edit';

import AdminReviewsIndex  from './pages/admin/reviews/index';
import AdminReviewsCreate from './pages/admin/reviews/create';
import AdminReviewsEdit   from './pages/admin/reviews/edit';

import AdminServicesDirectionsIndex  from './pages/admin/services-directions/index';
import AdminServicesDirectionsCreate from './pages/admin/services-directions/create';
import AdminServicesDirectionsEdit   from './pages/admin/services-directions/edit';

import AdminExpertLinksIndex  from './pages/admin/expert-links/index';
import AdminExpertLinksCreate from './pages/admin/expert-links/create';
import AdminExpertLinksEdit   from './pages/admin/expert-links/edit';

import AdminSocialMediasIndex  from './pages/admin/social-medias/index';
import AdminSocialMediasCreate from './pages/admin/social-medias/create';
import AdminSocialMediasEdit   from './pages/admin/social-medias/edit';

import AdminFormRequestsIndex from './pages/admin/form-requests/index';
import AdminFormRequestsShow  from './pages/admin/form-requests/show';

import AdminSiteInfoIndex from './pages/admin/site-info/index';
import AdminSiteInfoEdit  from './pages/admin/site-info/edit';

import PageNotFound from './pages/admin/not-found';

Vue.use(VueRouter);

export const routes = [
    {
        title:     'Главная',
        icon:      'mdi-view-dashboard',
        path:      "/admin",
        group:     false,
        isExact:   true,
        component: AdminIndex
    },
    {
        path:      '/admin/login',
        component: AdminLogin,
        meta:      {
            layout: 'admin-auth-layout'
        }
    },
    {
        title:     'Клиенты',
        icon:      'mdi-briefcase-account',
        path:      "/admin/clients",
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                path:      "",
                component: AdminClientsIndex
            },
            {
                path:      "create",
                component: AdminClientsCreate
            },
            {
                path:      ":id/edit",
                component: AdminClientsEdit
            }
        ]
    },
    {
        title:     'Проекты',
        icon:      'mdi-briefcase',
        path:      "/admin/projects/categories",
        component: AdminProjectsCategoriesIndex
    },
    {
        path:      "/admin/projects/category",
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                path:      "create",
                component: AdminProjectsCategoriesCreate
            },
            {
                path:      ":id/edit",
                component: AdminProjectsCategoriesEdit
            },
            {
                path:      ":id",
                component: AdminProjectsIndex
            }
        ]
    },
    {
        path:      "/admin/projects",
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                path:      "create",
                component: AdminProjectsCreate
            },
            {
                path:      ":id/edit",
                component: AdminProjectsEdit
            }
        ]
    },
    {
        title:     'Услуги',
        icon:      'mdi-beaker',
        path:      "/admin/services/categories",
        component: AdminServicesCategoriesIndex
    },
    {
        path:      "/admin/services/category",
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                path:      "create",
                component: AdminServicesCategoriesCreate
            },
            {
                path:      ":id/edit",
                component: AdminServicesCategoriesEdit
            },
            {
                path:      ":id",
                component: AdminServicesIndex
            }
        ]
    },
    {
        path:      "/admin/services",
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                path:      "create",
                component: AdminServicesCreate
            },
            {
                path:      ":id/edit",
                component: AdminServicesEdit
            }
        ]
    },
    {
        path:    '',
        divider: true
    },
    {
        title:     'Новости',
        icon:      'mdi-newspaper-variant',
        path:      "/admin/news",
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                path:      "",
                component: AdminNewsIndex
            },
            {
                path:      "create",
                component: AdminNewsCreate
            },
            {
                path:      ":id/edit",
                component: AdminNewsEdit
            }
        ]
    },
    {
        title:     'Статьи',
        icon:      'mdi-post',
        path:      "/admin/articles",
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                path:      "",
                component: AdminArticlesIndex
            },
            {
                path:      "create",
                component: AdminArticlesCreate
            },
            {
                path:      ":id/edit",
                component: AdminArticlesEdit
            }
        ]
    },
    {
        title:     'Видео',
        icon:      'mdi-video',
        path:      "/admin/videos",
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                path:      "",
                component: AdminVideosIndex
            },
            {
                path:      "create",
                component: AdminVideosCreate
            },
            {
                path:      ":id/edit",
                component: AdminVideosEdit
            }
        ]
    },
    {
        path:    '',
        divider: true
    },
    {
        title:     'Отзывы',
        icon:      'mdi-message-bulleted',
        path:      "/admin/reviews",
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                path:      "",
                component: AdminReviewsIndex
            },
            {
                path:      "create",
                component: AdminReviewsCreate
            },
            {
                path:      ":id/edit",
                component: AdminReviewsEdit
            }
        ]
    },
    {
        title:     'Направления услуг',
        icon:      'mdi-clipboard-list',
        path:      "/admin/services-directions",
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                path:      "",
                component: AdminServicesDirectionsIndex
            },
            {
                path:      "create",
                component: AdminServicesDirectionsCreate
            },
            {
                path:      ":id/edit",
                component: AdminServicesDirectionsEdit
            }
        ]
    },
    {
        title:     'Ссылки на новости',
        icon:      'mdi-link',
        path:      "/admin/expert-links",
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                path:      "",
                component: AdminExpertLinksIndex
            },
            {
                path:      "create",
                component: AdminExpertLinksCreate
            },
            {
                path:      ":id/edit",
                component: AdminExpertLinksEdit
            }
        ]
    },
    {
        path:    '',
        divider: true
    },
    {
        title:     'Заявки',
        icon:      'mdi-mailbox',
        path:      '/admin/form-requests',
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                path:      "",
                component: AdminFormRequestsIndex
            },
            {
                path:      ":id",
                component: AdminFormRequestsShow
            }
        ]
    },
    {
        title:     'Документы',
        icon:      'mdi-file-document',
        path:      "/admin/documents",
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                path:      "",
                component: AdminDocumentsIndex
            },
            {
                path:      "create",
                component: AdminDocumentsCreate
            },
            {
                path:      ":id/edit",
                component: AdminDocumentsEdit
            }
        ]
    },
    {
        title:     'Настройки',
        icon:      'mdi-settings',
        path:      "/admin/settings",
        isGroup:   true,
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children:  [
            {
                title:     'Пользователи',
                icon:      'mdi-account-multiple',
                path:      "/admin/users",
                component: {
                    render(c) {
                        return c("router-view");
                    }
                },
                children:  [
                    {
                        path:      "",
                        component: AdminUsersIndex
                    },
                    {
                        path:      "create",
                        component: AdminUsersCreate
                    },
                    {
                        path:      ":id/edit",
                        component: AdminUsersEdit
                    }
                ]
            },
            {
                title:     'Социальные сети',
                icon:      'mdi-forum',
                path:      "/admin/social-medias",
                component: {
                    render(c) {
                        return c("router-view");
                    }
                },
                children:  [
                    {
                        path:      "",
                        component: AdminSocialMediasIndex
                    },
                    {
                        path:      "create",
                        component: AdminSocialMediasCreate
                    },
                    {
                        path:      ":id/edit",
                        component: AdminSocialMediasEdit
                    }
                ]
            },
            {
                title:     'Настройки сайтов',
                icon:      'mdi-web',
                path:      "/admin/site-info",
                component: {
                    render(c) {
                        return c("router-view");
                    }
                },
                children:  [
                    {
                        path:      "",
                        component: AdminSiteInfoIndex
                    },
                    {
                        path:      ":id/edit",
                        component: AdminSiteInfoEdit
                    }
                ]
            }
        ]
    },
    {
        //  title:     'Test',
        icon:      'mdi-test-tube',
        path:      "/admin/test",
        group:     false,
        isExact:   true,
        component: AdminTest
    },
    {
        path:      "*",
        component: PageNotFound
    }
];

export default new VueRouter({
    mode: 'history',
    routes
});
