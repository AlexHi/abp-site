import {removeArrayItems} from "../../utils/removeArrayItems";

export function createSimpleArrayStore({fields, initialValue = []}) {
    const makeItem = (value) => {
        return fields.reduce((acc, {name, defaults = {value: null}}) => {
            acc[name] = {
                ...defaults,
                ...value[name]
            };
            return acc;
        }, {});
    };

    return {
        namespaced: true,

        state:     {
            value: initialValue
        },
        actions:   {
            reset({commit}, value) {
                commit('RESET', value);
            },
            add({commit}, item) {
                commit('ADD_ITEM', item);
            },
            setItemProperty({commit}, data) {
                commit('SET_ITEM_PROPERTY', data);
            },
            removeByIndex({commit}, index) {
                commit('REMOVE_ITEM_BY_INDEX', index);
            }
        },
        mutations: {
            RESET(state, value) {
                console.log('reset to', value, ':', value.map(makeItem))
                state.value = value.map(makeItem);
            },
            ADD_ITEM(state, initValues = {}) {
                state.value = [
                    ...state.value, makeItem(initValues)
                ];
            },
            REMOVE_ITEM_BY_INDEX(state, index) {
                state.value = removeArrayItems(state.value, index);
            },
            SET_ITEM_PROPERTY(state, {index, name, updates}) {
                state.value[index] = {
                    ...state.value[index],
                    [name]: {...state.value[index][name], ...updates}
                };
            }
        }
    };
}
