import Vue                      from 'vue';
import Vuex                     from 'vuex';
import {getUserInfo}            from "../api/getUserInfo";
import {createSimpleArrayStore} from "./common/createSimpleArrayStore";

Vue.use(Vuex);

export default new Vuex.Store({
    state:   {
        layout:               'admin-layout',
        userInfo:             null,
        backUrl:              null,
        clients:              [],
        users:                [],
        documents:            [],
        lastServicesCategory: null,
        lastProjectsCategory: null,
        servicesCategories:   [],
        projectsCategories:   [],
        loaders:              {
            clients:            false,
            users:              false,
            documents:          false,
            servicesCategories: false,
            projectsCategories: false
        }
    },
    getters: {
        layout(state) {
            return state.layout;
        },
        backUrl(state) {
            return state.backUrl;
        }
    },
    actions: {
        setBackUrl({commit}, url) {
            commit('setBackUrl', url);
        },
        async getUsers({commit}) {
            commit('setLoaderStatus', {
                name:   'users',
                status: true
            });

            await axios.get('/api/users/')
                .then(({data}) => {
                    if (data.status === 'success') {
                        commit('setUsers', data.data);
                    }

                    commit('setLoaderStatus', {
                        name:   'users',
                        status: false
                    });
                });
        },
        async getDocuments({commit}) {
            commit('setLoaderStatus', {
                name:   'documents',
                status: true
            });

            await axios.get('/api/documents/')
                .then(({data}) => {
                    if (data.status === 'success') {
                        commit('setDocuments', data.data);
                    }

                    commit('setLoaderStatus', {
                        name:   'documents',
                        status: false
                    });
                });
        },
        async getClients({commit}) {
            commit('setLoaderStatus', {
                name:   'clients',
                status: true
            });

            await axios.get('/api/clients/')
                .then(({data}) => {
                    if (data.status === 'success') {
                        commit('setClients', data.data);
                    }

                    commit('setLoaderStatus', {
                        name:   'clients',
                        status: false
                    });
                });
        },
        async getProjectsCategories({commit}) {
            commit('setLoaderStatus', {
                name:   'projectsCategories',
                status: true
            });

            await axios.get('/api/projects-categories/')
                .then(({data}) => {
                    if (data.status === 'success') {
                        commit('setProjectsCategories', data.data);
                    }

                    commit('setLoaderStatus', {
                        name:   'projectsCategories',
                        status: false
                    });
                });
        },
        setProjectsCategories({commit}, categories) {
            commit('setProjectsCategories', categories);
        },
        getLastProjectsCategory({commit}, {id}) {
            axios.get(`/api/projects-categories/${id}`)
                .then(({data}) => {
                    if (data.status === 'success') {
                        commit('setLastProjectsCategory', data.data);
                    }
                });
        },
        setLastProjectsCategory({commit}, category) {
            commit('setLastProjectsCategory', category);
        },
        async getServicesCategories({commit}) {
            commit('setLoaderStatus', {
                name:   'servicesCategories',
                status: true
            });

            await axios.get('/api/services-categories/')
                .then(({data}) => {
                    if (data.status === 'success') {
                        commit('setServicesCategories', data.data);
                    }

                    commit('setLoaderStatus', {
                        name:   'servicesCategories',
                        status: false
                    });
                });
        },
        setServicesCategories({commit}, categories) {
            commit('setServicesCategories', categories);
        },
        getLastServicesCategory({commit}, {id}) {
            axios.get(`/api/services-categories/${id}`)
                .then(({data}) => {
                    if (data.status === 'success') {
                        commit('setLastServicesCategory', data.data);
                    }
                });
        },
        setLastServicesCategory({commit}, category) {
            commit('setLastServicesCategory', category);
        },
        getUserInfo({commit}) {
            getUserInfo()
                .then(({data}) => {
                    commit('setUserInfo', data.data);
                });
        }
    },

    mutations: {
        setLoaderStatus(state, {name, status}) {
            state.loaders[name] = status;
        },
        setUserInfo(state, userInfo) {
            state.userInfo = userInfo;
        },
        setLayout(state, payload) {
            state.layout = payload;
        },
        setBackUrl(state, url) {
            state.backUrl = url;
        },
        setClients(state, clients) {
            state.clients = clients;
        },
        setUsers(state, users) {
            state.users = users;
        },
        setDocuments(state, documents) {
            state.documents = documents;
        },
        setLastServicesCategory(state, category) {
            state.lastServicesCategory = category;
        },
        setServicesCategories(state, categories) {
            state.servicesCategories = categories;
        },
        setLastProjectsCategory(state, category) {
            state.lastProjectsCategory = category;
        },
        setProjectsCategories(state, categories) {
            state.projectsCategories = categories;
        }
    },

    modules: {
        service_sub_services:     createSimpleArrayStore({
            fields:       [
                {
                    name:     'name',
                    defaults: {
                        value: '',
                        attrs: {
                            label: 'Название'
                        }
                    }
                },
                {
                    name:     'sort',
                    defaults: {
                        value: 100,
                        attrs: {
                            label: 'Сортировка',
                            type:  'number'
                        }
                    }
                },
                {
                    name:     'price',
                    defaults: {
                        attrs: {
                            label: 'Цена'
                        }
                    }
                }
            ],
            initialValue: []
        }),
        service_steps:            createSimpleArrayStore({
            fields:       [
                {
                    name:     'name',
                    defaults: {
                        value: '',
                        attrs: {
                            label: 'Название'
                        }
                    }
                },
                {
                    name:     'sort',
                    defaults: {
                        value: 100,
                        attrs: {
                            label: 'Сортировка',
                            type:  'number'
                        }
                    }
                },
                {
                    name:     'time',
                    defaults: {
                        attrs: {
                            label: 'Срок'
                        }
                    }
                }
            ],
            initialValue: []
        }),
        services_directions_list: createSimpleArrayStore({
            fields:       [
                {
                    name:     'name',
                    defaults: {
                        value: '',
                        attrs: {
                            label: 'Название'
                        }
                    }
                }
            ],
            initialValue: []
        }),
        profile_specialisation: createSimpleArrayStore({
            fields:       [
                {
                    name:     'text',
                    defaults: {
                        value: '',
                        attrs: {
                            label: 'Текст'
                        }
                    }
                }
            ],
            initialValue: []
        }),
        test:                     createSimpleArrayStore({
            fields:       [
                {
                    name:     'text',
                    defaults: {
                        value: '',
                        attrs: {
                            label: 'test'
                        }
                    }
                },
                {
                    name:     'icon',
                    defaults: {
                        value: '',
                        attrs: {
                            label: 'test'
                        }
                    }
                },
                {
                    name:     'age',
                    defaults: {
                        value: 10,
                        attrs: {
                            label: 'age',
                            type:  'number'
                        }
                    }
                }
            ],
            initialValue: []
        })
    }
});
