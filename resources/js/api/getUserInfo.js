export const getUserInfo = () => axios.get('/api/user')
    .then(response => {
        if (response.data.status !== 'success') {
            throw new Error(response.data.error ?? 'unknown error');
        }
        return response;
    });
