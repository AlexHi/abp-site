export const deleteEntity = (apiLink, id) =>
    axios.delete(`${apiLink}/${id}`, {
            headers: {
                "Content-type": "multipart/form-data"
            }
        })
        .catch((e) => {
            if (e.response.data.errors) {
                const error = Object.values(e.response.data.errors).reduce((acc, e) => acc + ' ' + e, '');
                throw new Error(error);
            }
            throw new Error(e);
        })
        .then(response => {
            if (response.data.status !== 'success') {
                throw new Error(response.data.desc ?? 'unknown error');
            }
            return response;
        });
