import Vue     from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

import en from 'vuetify/es5/locale/en';
import ru from 'vuetify/es5/locale/ru';

const opts = {
    lang:  {
        locales: {
            en,
            ru
        },
        current: 'ru'
    },
    theme: {
        themes: {
            dark: {
                primary: '#005ea9'
            }
        }
    }
};

export default new Vuetify(opts);
