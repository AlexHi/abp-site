export default {
    defaultAddition: '',
    install(Vue, options) {
        if (options) {
            if (options.defaultAddition) {
                this.defaultAddition = options.defaultAddition;
            }
        }

        Vue.prototype.$title = this;
    },

    set(title) {
        document.title = `${title}${this.defaultAddition}`;
    }
};
