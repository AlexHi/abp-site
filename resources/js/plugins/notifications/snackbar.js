export default {
    namespaced: true,
    state:      () => ({
        message: '',
        options: null
    }),
    actions:    {
        setSnack({commit}, {message, options}) {
            commit('setSnack', message);
            commit('setOptions', options);
        }
    },
    mutations:  {
        setSnack(state, message) {
            state.message = message;
        },
        setOptions(state, options) {
            state.options = options;
        }
    }
};
