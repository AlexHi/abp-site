import snackbar from './snackbar';

export default {
    install(Vue, options) {
        if (!options || !options.store) {
            throw new Error("Please provide vuex store.");
        }

        options.store.registerModule('snackbar', snackbar);

        this.store = options.store;

        Vue.prototype.$notify = this;
    },

    info(message, timeout = 5000) {
        this.store.dispatch('snackbar/setSnack', {
            message,
            options: {
                type:    'info',
                timeout: timeout
            }
        });
    },

    success(message, timeout = 5000) {
        this.store.dispatch('snackbar/setSnack', {
            message,
            options: {
                type:    'success',
                timeout: timeout
            }
        });
    },

    warning(message, timeout = 5000) {
        this.store.dispatch('snackbar/setSnack', {
            message,
            options: {
                type:    'warning',
                timeout: timeout
            }
        });
    },

    error(message, timeout = 5000) {
        this.store.dispatch('snackbar/setSnack', {
            message,
            options: {
                type:    'error',
                timeout: timeout
            }
        });
    }
};
