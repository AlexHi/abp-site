export const removeArrayItems = (array, index) => {
    const copy = [...array];

    copy.splice(index, 1);

    return copy;
};
