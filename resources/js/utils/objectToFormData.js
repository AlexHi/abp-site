export const objectToFormData = (object) => {
    let formData = new FormData();

    Object.keys(object).forEach(key => {
        if (object[key] !== null && object[key] !== 'null' && typeof object[key] !== 'undefined' && typeof object[key] !== undefined) {
            formData.append(key, object[key]);
        }
    });

    return formData;
};
