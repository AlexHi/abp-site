import router   from "./router";
import store    from "./store/store";
import vuetify  from "./plugins/vuetify";
import Notify   from "./plugins/notifications/notification";
import Title    from "./plugins/title";
import CKEditor from "@ckeditor/ckeditor5-vue";

require('./plugins/axios');

window.Vue = require('vue');

Vue.use(Title, {
    default:         'Панель управления',
    defaultAddition: ' | Панель управления'
});
Vue.use(Notify, {store});
Vue.use(CKEditor);

Vue.component('app', require('./components/App').default);
Vue.component('admin-auth-layout', require('./layouts/admin-auth-layout').default);
Vue.component('admin-layout', require('./layouts/admin-layout/layout').default);
Vue.component('site-admin-panel', require('./components/admin/site-admin-panel').default);

const app = new Vue({
    el: '#app',
    vuetify,
    router,
    store
});
