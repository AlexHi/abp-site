<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <link href="{{ mix('css/style.min.css') }}" rel="stylesheet">
        <!-- Styles -->
    </head>
    <body class="antialiased font-sans">
    <div class="wrapper">
        <section class="error-404 error-common">
        <div class="container">
            <div class="error-common__row">
            <div class="error-common__bg">!!!</div>
            <div class="error-common__name">@yield('code', __('Oh no'))</div>
          <div class="error-common__subname">@yield('message')</div>
          <div class="error-common__text">Свяжитесь с нами:</div>
          <a href="+74999907235" class="error-common__phone">+7 (499) 990-72-35</a>
          <a href="mailto:abp@abp.legal" class="error-common__mail">abp@abp.legal</a>
            </div>
        </div>
        </section>
    </div>
        </div>
    </body>
</html>
