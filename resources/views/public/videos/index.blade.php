@extends('layouts.app')

@section('pageTitle', __('videos.title'))

@section('content')
    <section class="video">
        <div class="container-wrap">
            <h2 class="video__title title">{{__('videos.title_index_h1')}}</h2>
            <div class="video__row-1">
                <div class="video__entry">
                    @foreach($videos as $video)
                        <div class="video__item">
                            <a class="video__prev" href="/videos/{{$video->id}}" target="_blank">
                                <img src="https://img.youtube.com/vi/{{$video->youtube_code}}/maxresdefault.jpg"
                                     alt="{{$video->name}}">
                            </a>

                            <div class="video__name">
                                <a href="/videos/{{$video->id}}" target="_blank">{{$video->name}}</a>
                            </div>

                            <div class="video__text">
                                {{$video->description}}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="video__row-2">
                {{$videos->links('components.pagination')}}
            </div>
        </div>
    </section>
@endsection
