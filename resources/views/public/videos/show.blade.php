@extends('layouts.app')

@section('pageTitle', $video->name)

@section('ogTitle', $video->name)
@section('ogDescription', $video->description)
@section('ogImage', "https://img.youtube.com/vi/".$video->youtube_code."/maxresdefault.jpg")
@section('ogType', 'video')
@section('ogUrl', Request::fullUrl())

@section('content')
    <section class="article article-video">
        <div class="container-wrap">
            <div class="article__row">
                <h2 class="article__title title">{{$video->name}}</h2>
                <div class="article__column">
                    <div class="article__left">
                        <iframe width="100%" height="100%"
                                src="https://www.youtube.com/embed/{{$video->youtube_code}}"
                                allowfullscreen="true" webkitallowfullscreen="true"
                                mozallowfullscreen="true" frameborder="0">
                        </iframe>

                        <div class="article__text">
                            {!! $video->description !!}
                        </div>
                    </div>
                    <div class="article__right">

                        <div class="article__group">
                            @if($video->created_at)
                                <div class="article__date">{{$video->created_at->formatLocalized('%d.%m.%Y')}}</div>
                            @endif
                            <div class="article__share">
                                <div class="ya-share2" data-curtain data-shape="round" data-color-scheme="whiteblack"
                                     data-lang="{{env('APP_LOCALE')}}" data-limit="0" data-more-button-type="long"
                                     data-services="vkontakte,facebook,odnoklassniki,telegram,twitter,viber,whatsapp,skype,linkedin"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="article__row">
                <a href="/videos" class="article__back"><span>{{__('videos.back_link')}}</span></a>
                <div class="article__column">
                    @if(isset($videos))
                        @foreach($videos as $video)
                            <a class="article__wrap" target="_blank" href="/videos/{{$video->id}}">
                                <div class="publication-1">
                                    <div class="publication-1__group">
                                        <div class="publication-1__date">
                                            @if($video->created_at)
                                                {{$video->created_at->formatLocalized('%d.%m.%Y')}}
                                            @endif
                                        </div>
                                        <div class="publication-1__name">{{$video->name}}</div>
                                    </div>
                                    <img class="publication-1__image"
                                         src="https://img.youtube.com/vi/{{$video->youtube_code}}/maxresdefault.jpg"
                                         alt="{{$video->name}}">
                                </div>
                            </a>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

