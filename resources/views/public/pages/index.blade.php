@extends('layouts.app')

@section('pageTitle', $page->title)

@section('content')
    <section class="page">
        <div class="container-wrap">
            <h2 class="page__title title">{{$page->title}}</h2>
            <div class="page__body">
                {!! $page->body !!}
            </div>
        </div>
    </section>
@endsection
