@extends('layouts.app')

@section('pageTitle', __('clients.title'))

@section('content')
    <section class="clients">
        <div class="container-wrap">
            <div class="clients__row">
                <h2 class="analytics__title title">{{__('clients.title_index_h1')}}</h2>
                <div class="clients__column">
                    @foreach($clients as $client)
                        <a class="clients__item" href="{{$client->link}}" target="_blank" rel="noreferrer">
                            <img src="/storage/{{$client->logo}}" alt="{{$client->name}}">
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
