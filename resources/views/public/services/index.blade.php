@extends('layouts.app')

@section('pageTitle', __('services.title'))

@section('content')
    <section class="support">
        <div class="container-wrap">
            <div class="support__row">
                <h2 class="support__title title">{{__('services.title_index_h1')}}</h2>
                <div class="support__tabs tabs-support">
                    <div class="tabs-support__controls">
                        @foreach($services_categories as $category)
                            <a id="category-{{$category->id}}-tab" href="#tabs-support-{{$category->id}}"
                               class="tabs-support__button tab-btn">
                                <img src="/storage/{{$category->icon}}" alt="{{$category->name}}"/>
                                <span>{{$category->name}}</span>
                            </a>
                        @endforeach

                    </div>
                    <div class="tabs-support__content">
                        @foreach($services_categories as $category)
                            <div id="tabs-support-{{$category->id}}" class="tabs-support__item tab-item">
                                <div class="tabs-support__text">{{__('services.subtitle_index')}}</div>
                                <div class="tabs-support__list-2">
                                    <div class="tabs-support__column">

                                        @foreach($category->services as $service)

                                            <div class="tabs-support__element-2">
                                                <a href="/services/{{$service->id}}">
                                                    {{$service->name}}
                                                    <br/>
                                                    <span>{{$service->minimal_price}}</span>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('components.form', ['form_name' => 'questions'])

@endsection
