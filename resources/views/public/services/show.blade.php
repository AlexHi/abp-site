@extends('layouts.app')

@section('pageTitle', $service->name)

@section('ogTitle', $service->name)
@section('ogImage', env('APP_URL').'/storage/'. $site_info->header_logo)
@section('ogType', 'service')
@section('ogUrl', Request::fullUrl())

@section('content')
    <section class="service-item">
        <div class="container-wrap">

            <div class="service-item__row">
                <div class="service-item__column">
                    <h2 class="service-item__title title">{{$service->name}}</h2>
                    <div class="service-item__text">
                        {!! $service->text !!}
                    </div>
                    <div class="service-item__text-2">{{__("services.term_of_the_work")}}</div>
                    <div class="service-item__wrap">
                        @php
                            $steps = json_decode($service->steps, true);
                            if($steps && count($steps) > 0) {
                               usort($steps, function ($item1, $item2) {
                                    return $item1['sort'] <=> $item2['sort'];
                               });
                            }
                        @endphp
                        @foreach($steps as $step)
                            @if(isset($step['name']))
                                <div class="service-item__group">
                                    <div class="service-item__step">{{$loop->index+1}}</div>
                                    <div class="service-item__group-info">
                                        <div class="service-item__stage">{{$step['name']}}</div>
                                        @if(isset($step['time']))
                                            <div class="service-item__time">{{$step['time']}}</div>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="service-item__column">
                    <div class="service-item__frame">
                        <div class="service-item__name">{{__('services.service_price')}}</div>
                        <div class="service-item__group-2">
                            @php
                                $sub_services = json_decode($service->sub_services, true);
                                if($sub_services && count($steps) > 0) {
                                   usort($sub_services, function ($item1, $item2) {
                                        return $item1['sort'] <=> $item2['sort'];
                                   });
                                }
                            @endphp
                            @foreach($sub_services as $sub_service)
                                @if(isset($sub_service['name']))
                                    <div class="service-item__text-3">
                                        {{$sub_service['name']}}
                                        @if(isset($sub_service['price']))
                                            <div>{{$sub_service['price']}}</div>
                                        @endif
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        @if($service->total_price)
                            <div class="service-item__group-3">
                                <div class="service-item__text-3 service-item__text-3_bold">
                                    {{__("services.total")}}
                                    <div>
                                        @if(count($total_price_elements) > 1)
                                            @foreach($total_price_elements as $price)
                                                <span>{{$price}}</span>
                                            @endforeach
                                        @else
                                            {{ $service->total_price }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="service-item__text-3 service-item__text-3_large-fz">
                        {{$service->price_note}}
                    </div>
                    <a href="#" id="modal-button"
                       class="service-item__button js-modal">{{__("services.service_request_button")}}</a>
                </div>
            </div>
            <div class="article__row">
                <a href="/services" class="article__back"><span>{{__("services.back_link")}}</span></a>
            </div>
        </div>
    </section>

    <div class="modal">
        <div class="modal__body">
            <form action="/form/service_order#form" id="form-request" class="modal__form form-modal"
                  enctype="multipart/form-data"
                  method="post">
                @csrf
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                <input type="hidden" name="thing_url" value="{{Request::fullUrl()}}"/>
                <input type="hidden" name="thing_name" value="{{$service->name}}"/>
                <div class="form-modal__close"></div>
                <div class="form-modal__title">{{__("services.form_title")}}</div>
                <div class="form-modal__group">
                    <div class="form-modal__name">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                        @enderror
                        <input type="text" name="name" placeholder="{{ __('forms.name_placeholder') }}"/>
                    </div>

                    <div class="form-modal__email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                        @enderror
                        <input type="email" name="email" placeholder="{{ __('forms.email_placeholder') }}"/>
                    </div>
                </div>

                <div class="form-modal__comment">
                    @error('message')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                    @enderror
                    <textarea name="message" placeholder="{{ __('forms.question_placeholder') }}"></textarea>
                </div>

                <label class="form-modal__check checkbox-form">
                    @error('agreement')
                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('agreement') }}</strong>
                                        </span>
                    @enderror
                    {{ __('forms.agreement') }}
                    @if($site_info->pdppDocument)
                        <a href="/storage/{{$site_info->pdppDocument->file}}" download>
                            {{ __('forms.agreement_file') }}
                        </a>
                    @else
                        {{ __('forms.agreement_file') }}
                    @endif
                    <input type="checkbox" name="agreement">
                    <span class="checkbox-form__checkmark"></span>
                </label>

                <button class="g-recaptcha form-modal__submit"
                        data-sitekey="{{env('RECAPTCHA_SITE_KEY')}}"
                        data-callback='onSubmit'>
                    {{ __('forms.button') }}
                </button>
            </form>
        </div>
        <div class="modal__overlay"></div>
    </div>
@endsection
