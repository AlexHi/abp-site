@extends('layouts.app')

@section('pageTitle', $teamUser->fullName())

@section('ogTitle', $teamUser->fullName())
@section('ogDescription', $teamUser->description)
@section('ogImage', env('APP_URL').'/storage/'.$teamUser->profile->image)
@section('ogType', 'team')
@section('ogUrl', Request::fullUrl())

@section('content')
    <section class="person">
        <div class="person__row">
            <div class="container-wrap">
                <div class="person__group">
                    <div class="person__column">

                        <div class="person__image">
                            <img src="/storage/{{$teamUser->profile->image}}" alt="{{$teamUser->fullName()}}"/>
                        </div>

                        <div class="person__group-question">
                            <div class="person__button">
                                <a href="#" id="modal-button"
                                   class="btn js-modal"><span>{{__('teams.ask_a_question')}}</span></a>
                            </div>

                            @if($teamUser->profile->document)
                                <div class="person__degree">
                                    <a href="/storage/{{$teamUser->profile->document->file}}" download>
                                        {{$teamUser->profile->document->name}}
                                    </a>
                                </div>
                            @endif
                        </div>

                    </div>
                    <div class="person__column">
                        <div class="person__wrap person__wrap_one">
                            <div class="person__name">{{$teamUser->fullName()}}</div>
                            <div class="person__post">{{$teamUser->profile->work_position}}</div>
                        </div>
                        <div class="person__wrap person__wrap_two">
                            <div class="person__group-item">
                                <div class="person__text-2"> {{$teamUser->profile->description}}</div>
                            </div>
                        </div>
                        <div class="person__wrap person__wrap_two">
                            <div class="person__group-info">

                                <div class="person__group-item">
                                    <div class="person__text">{{__('teams.education')}}</div>
                                    <div class="person__text-2">{{$teamUser->profile->education}}</div>
                                </div>

                                <div class="person__group-item">
                                    <div class="person__text">{{__('teams.language_skills')}}</div>
                                    <div class="person__text-2">{{$teamUser->profile->languages}}</div>
                                </div>

                                <div class="person__group-item">
                                    <div class="person__text">{{__('teams.contacts')}}</div>
                                    <div class="person__text-2"><a
                                            href="mailto:{{$teamUser->email}}">{{$teamUser->email}}</a></div>
                                    <div class="person__text-2"><a
                                            href="tel:{{$teamUser->profile->phone_number}}">{{$teamUser->profile->phone_number}}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="person__group-info">
                                @if($teamUser->profile->specialisation && json_decode($teamUser->profile->specialisation, true))
                                    <div class="person__text">{{__('teams.specialisation')}}</div>
                                    <div class="person__list">
                                        @foreach(json_decode($teamUser->profile->specialisation, true) as $specialisation)
                                            @if(isset($specialisation['text']))
                                                <div class="person__item">
                                                    {{$specialisation['text']}}
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="person__row">
            <div class="container-wrap">
                @if(count($teamUser->projects) > 0)
                    <h3 class="person__title title">{{__('teams.projects')}}</h3>
                    <div class="person__group-2">
                        <div class="person__slider-1">
                            <div class="swiper-wrapper">
                                @foreach($teamUser->projects as $project)
                                    <div class="person__slide-1 swiper-slide">
                                        <div class="entry__item">
                                            <div class="entry__image">
                                                <img src="/storage/{{$project->client->logo}}"
                                                     alt="{{$project->client->name}}"/>
                                            </div>
                                            <div class="entry__group">
                                                <div class="entry__data">{{$project->client->name}}</div>
                                                <div class="entry__name">
                                                    @if(mb_strlen($project->description) > 70)
                                                        {{mb_substr($project->description, 0, 70)}}...

                                                    @else
                                                        {{$project->description}}
                                                    @endif
                                                </div>
                                                <div class="entry__btn">
                                                    <a href="/projects/{{$project->id}}"><span>{{__('teams.projects_more')}}</span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="person__arrow-2">
                                <div class="person__button-prev"></div>
                                <div class="person__dots-2"></div>
                                <div class="person__button-next"></div>
                            </div>
                        </div>
                    </div>
                @endif
                @if(count($teamUser->posts) > 0)
                    <h3 class="person__title title">{{__('teams.publications')}}</h3>
                    <div class="person__group-2">
                        <div class="person__slider-2">

                            <div class="swiper-wrapper">
                                @foreach($teamUser->posts as $post)
                                    <div class="person__slide-2 swiper-slide">
                                        <div class="entry__item">
                                            <div class="entry__image">
                                                <a href="/{{$post->type}}/{{$post->id}}">
                                                    <img src="/storage/{{$post->image}}" alt=""/>
                                                </a>
                                            </div>
                                            <div class="entry__group">
                                                <div
                                                    class="entry__data">{{$post->created_at->formatLocalized('%d.%m.%Y')}}</div>
                                                <div class="entry__name"><a
                                                        href="/{{$post->type}}/{{$post->id}}">{{$post->title}}</a>
                                                </div>
                                                <div class="entry__discription">{{$post->description}}...</div>
                                                <div class="entry__btn">
                                                    <a href="/{{$post->type}}/{{$post->id}}"><span>{{__('teams.publications_more')}}</span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="person__arrow-2">
                                <div class="person__button-prev"></div>
                                <div class="person__dots-2"></div>
                                <div class="person__button-next"></div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
    <div class="modal">
        <div class="modal__body">
            <form action="/form/teams_expert_question#form" id="form-request" class="modal__form form-modal"
                  enctype="multipart/form-data"
                  method="post">
                @csrf
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                <input type="hidden" name="user_email" value="{{$teamUser->email}}"/>
                <input type="hidden" name="thing_url" value="{{Request::fullUrl()}}"/>
                <input type="hidden" name="thing_name" value="{{$teamUser->fullName()}}"/>
                <div class="form-modal__close"></div>
                <div class="form-modal__title">{!! __('teams.ask_an_expert') !!}</div>
                <div class="form-modal__group">
                    <div class="form-modal__name">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                        @enderror
                        <input type="text" name="name" placeholder="{{ __('forms.name_placeholder') }}"/>
                    </div>

                    <div class="form-modal__email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                        @enderror
                        <input type="email" name="email" placeholder="{{ __('forms.email_placeholder') }}"/>
                    </div>
                </div>

                <div class="form-modal__comment">
                    @error('message')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                    @enderror
                    <textarea name="message" placeholder="{{ __('forms.question_placeholder') }}"></textarea>
                </div>

                <label class="form-modal__check checkbox-form">
                    @error('agreement')
                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('agreement') }}</strong>
                                        </span>
                    @enderror
                    {{ __('forms.agreement') }}
                    @if($site_info->pdppDocument)
                        <a href="/storage/{{$site_info->pdppDocument->file}}" download>
                            {{ __('forms.agreement_file') }}
                        </a>
                    @else
                        {{ __('forms.agreement_file') }}
                    @endif
                    <input type="checkbox" name="agreement">
                    <span class="checkbox-form__checkmark"></span>
                </label>

                <button class="g-recaptcha form-modal__submit"
                        data-sitekey="{{env('RECAPTCHA_SITE_KEY')}}"
                        data-callback='onSubmit'>
                    {{ __('forms.button') }}
                </button>
            </form>
        </div>
        <div class="modal__overlay"></div>
    </div>

@endsection
