@extends('layouts.app')

@section('pageTitle', __('teams.title'))

@section('content')
    <section class="team">
        <div class="container-wrap">
            <div class="team__row">
                <h2 class="team__title title">{{__('teams.our_team')}}</h2>
                <div class="team__list">
                    @foreach($users as $user)
                        <div class="team__item item-team">
                            <a href="/team/{{$user->id}}" class="item-team__wrap">
                                <div class="item-team__image ">
                                    <svg>
                                        <defs>
                                            <clipPath id="clipping-partial">
                                                <rect width="300px" height="300px" x="60px" y="50px"/>
                                            </clipPath>

                                            <clipPath id="clipping-mobile">
                                                <rect width="225px" height="225px" x="35px" y="38px"/>
                                            </clipPath>

                                            <clipPath id="clipping-full">
                                                <rect width="100%" height="100%" x="0" y="0"/>
                                            </clipPath>
                                        </defs>
                                        <image style="width: 100%; height: 100%; object-fit: cover;" width="100%"
                                               height="100%"
                                               preserveAspectRatio="xMidYMid slice"
                                               xlink:href="/storage/{{$user->profile->image}}"/>
                                    </svg>
                                </div>
                                <div class="item-team__space"></div>
                                <div class="item-team__group">
                                    <div class="item-team__name">{{$user->fullName()}}</div>
                                    <div class="item-team__post">{{$user->profile->work_position}}</div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @include('components.form', ['form_name' => 'questions'])
@endsection
