<section class="publish">
    <div class="container-wrap">
        <div class="publish__row">
            <h2 class="publish__title title">{{ __('main.publish_title') }}</h2>

            <div class="publish__tabs tabs">
                <div class="tabs__control">
                    <a href="#tab-1" class="tabs__button tab-btn">{{ __('main.publish_news_title') }}</a>
                    <a href="#tab-2" class="tabs__button tab-btn">{{ __('main.publish_videos_title') }}</a>
                    <a href="#tab-3" class="tabs__button tab-btn">{{ __('main.publish_articles_title') }}</a>
                </div>
                <div class="tabs__content">
                    <div id="tab-1" class="tabs__item tab-item">
                        <div class="tabs__wrap">
                            @foreach($news as $news_item)
                                <a href="/news/{{$news_item->id}}" class="tabs__publication publication-1"
                                   target="_blank">
                                    <div class="publication-1__group">
                                        <div
                                            class="publication-1__date">{{$news_item->created_at->formatLocalized('%d.%m.%Y')}}</div>
                                        <div class="publication-1__name">
                                            {{$news_item->title}}
                                        </div>
                                    </div>
                                    <img class="publication-1__image" src="/storage/{{$news_item->image}}"
                                         alt="{{$news_item->title}}"/>
                                </a>
                            @endforeach
                        </div>
                        <div class="publish__button">
                            <a class="btn" href="/news"><span>{{ __('main.publish_news_button') }}</span></a>
                        </div>
                    </div>

                    <div id="tab-2" class="tabs__item tab-item">
                        <div class="tabs__wrap">

                            <div class="tabs__publication publication-2">

                                @foreach($videos as $video)
                                    <div class="publication-2__wrap">
                                        <iframe width="100%" height="100%"
                                                src="https://www.youtube.com/embed/{{$video->youtube_code}}"
                                                allowfullscreen="true" webkitallowfullscreen="true"
                                                mozallowfullscreen="true" frameborder="0"></iframe>

                                        <div class="publication-2__prew-image">
                                            <img src="https://img.youtube.com/vi/{{$video->youtube_code}}/maxresdefault.jpg"
                                                 alt="{{$video->name}}">
                                        </div>

                                        <div class="publication-2__name">
                                            {{$video->name}}
                                        </div>

                                        <div class="publication-2__text">
                                            {{$video->description}}
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div class="tabs__publication publication-2 js-image-group">
                                <img class="publication-2__preview js-prew-1" src="" alt="">
                                <img class="publication-2__preview js-prew-1" src="" alt="">
                                <img class="publication-2__preview js-prew-1" src="" alt="">
                            </div>

                            <div class="tabs__publication publication-2 js_image-group">
                                <img class="publication-2__preview js-prew-2" src="" alt="">
                                <img class="publication-2__preview js-prew-2" src="" alt="">
                                <img class="publication-2__preview js-prew-2" src="" alt="">
                            </div>


                            <div class="tabs__publication publication-2">
                                <div class="publication-2__discription">
                                    <div class="publication-2__name"></div>
                                    <div class="publication-2__text"></div>
                                </div>
                            </div>
                        </div>

                        <div class="publish__button">
                            <a class="btn" href="/videos"><span>{{ __('main.publish_videos_button') }}</span></a>
                        </div>
                    </div>

                    <div id="tab-3" class="tabs__item tab-item">
                        <div class="tabs__wrap">
                            @foreach($articles as $article)
                                <a href="/articles/{{$article->id}}" class="tabs__publication publication-1"
                                   target="_blank">
                                    <div class="publication-1__group">
                                        <div
                                            class="publication-1__date">{{$article->created_at->formatLocalized('%d.%m.%Y')}}</div>
                                        <div class="publication-1__name">
                                            {{$article->title}}
                                        </div>
                                    </div>
                                    <img class="publication-1__image" src="/storage//{{$article->image}}"
                                         alt="{{$article->title}}"/>
                                </a>
                            @endforeach
                        </div>
                        <div class="publish__button">
                            <a class="btn" href="/articles"><span>{{ __('main.publish_articles_button') }}</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
