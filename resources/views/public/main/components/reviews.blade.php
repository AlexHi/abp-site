<section class="reviews">
    <div class="container-wrap">
        <div class="reviews__row">
            <h2 class="reviews__title title">{{ __('main.reviews_title') }}</h2>
            <div class="reviews__column">
                <div class="reviews__slider-1 slider-1">

                    <div class="swiper-wrapper">
                    @foreach($reviews as $review)
                        <div class="slider-1__slide swiper-slide">
                            <div class="slider-1__post">{{$review->work_position}}</div>
                            <div class="slider-1__firm">{{$review->name}}</div>
                            <div class="slider-1__coment">
                                {{$review->text}}
                            </div>
                        </div>
                    @endforeach
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>

                <div class="reviews__slider-2 slider-2 swiper-container">

                    <div class="swiper-wrapper">
                    @foreach($reviews as $review)
                        <div class="slider-2__slide swiper-slide">
                            <img src="/storage/{{$review->logo}}" alt="{{$review->name}}"/>
                        </div>
                    @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
