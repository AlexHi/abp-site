<section class="services">
    <div class="container-wrap">
        <div class="services__row">
            <h2 class="services__title title">{{ __('main.services_title') }}</h2>
            <div class="services__column">
                <div class="swiper-wrapper">
                    @foreach($servicesDirections as $direction)
                        @php
                            $href = '';

                            if($direction->link && $direction->link !== '') {
                                $href=$direction->link;
                            } else if ($direction->category) {
                                $href = '/services#category-' . $direction->category->id;
                            }
                        @endphp
                        @if($href !== '')
                            <a class="services__wrap swiper-slide" href="{{$href}}" target="_blank">
                                @else
                                    <div class="services__wrap swiper-slide">
                                        @endif
                                        <div class="services__item">
                                            <div class="services__group">
                                                <div class="services__logo">
                                                    <img src="/storage/{{$direction->icon}}"
                                                         alt="{{$direction->name}}"/>
                                                </div>
                                                <div class="services__name">{{$direction->name}}</div>
                                            </div>

                                            @foreach(json_decode($direction->services_list, true) as $service)
                                                @if(isset($service['name']))
                                                    <div class="services__text">{{$service['name']}}</div>
                                                @endif
                                            @endforeach
                                        </div>
                                    @if($href !== '')
                            </a>
                        @else
                </div>
                @endif
                @endforeach
            </div>
            <div class="services__dots"></div>
        </div>
        <div class="services__button">
            <a class="btn" href="/services"><span>{{ __('main.services_button') }}</span></a>
        </div>
    </div>
    </div>
</section>
