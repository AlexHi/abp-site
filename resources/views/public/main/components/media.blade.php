<section class="mass-media">
    <div class="container-wrap">
        <div class="mass-media__row">
            <h2 class="mass-media__title title">{{ __('main.media_title') }}</h2>
            <div class="mass-media__column">
                <div class="mass-media__slider">
                    <div class="swiper-wrapper">
                    @foreach($expert_links as $expert_link)
                        <div class="mass-media__slide swiper-slide">
                            <a href="{{$expert_link->link}}" target="_blank" rel="noreferrer">
                                <img src="/storage/{{$expert_link->logo}}" alt="{{$expert_link->name}}"/>
                            </a>
                        </div>
                    @endforeach
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
        </div>
    </div>
</section>
