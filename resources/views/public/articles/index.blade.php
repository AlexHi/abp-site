@extends('layouts.app')

@php
    $pageString = (PaginateRoute::currentPage() > 1) ? ' ' . PaginateRoute::currentPage() : '';
@endphp

@section('pageTitle', __('articles.title') . $pageString)

@section('content')
    <section class="analytics">
        <div class="container-wrap">
            <div class="analytics__row">
                <h2 class="analytics__title title">{{__('articles.title_index_h1')}}</h2>
                <div class="analytics__column">
                    @foreach($posts as $post)
                        <a href="/articles/{{$post->id}}" class="analytics__entry">
                            <div class="analytics__group-description">
                                <div class="analytics__name">{{$post->title}}</div>
                                <div
                                    class="analytics__data">{{$post->created_at->formatLocalized('%d.%m.%Y')}}</div>
                                <div class="analytics__discription">
                                    {{$post->description}}
                                </div>
                            </div>
                            <div class="analytics__group-person">
                                <div class="article__team team-article">
                                    <div class="team-article__img"><img
                                            src="/storage/{{$post->user->profile->image}}"
                                            alt="{{$post->user->fullName()}}"/></div>
                                    <div class="team-article__group-person">
                                        <div class="team-article__name">{{$post->user->fullName()}}</div>
                                        <div
                                            class="team-article__post">{{$post->user->profile->work_position}}</div>
                                        <div class="team-article__email">{{$post->user->email}}</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
            <div class="analytics__row">
                {{$posts->links('components.pagination')}}
            </div>
        </div>
    </section>
@endsection
