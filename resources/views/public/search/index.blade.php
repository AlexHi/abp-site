@extends('layouts.app')

@section('pageTitle', __('search.title'))

@section('content')
    <section class="search">
        <div class="container-wrap">
            <div class="search__row">
                <h2 class="search__title title">{{__('search.title_h1')}}</h2>
                <div class="search__wrap">
                    <form action="/search/" method="GET">
                        <div class="search__group-input">
                            <label>
                                <input class="search__value" name="search" value="{{$search_text}}"
                                       placeholder="{{__('search.input_placeholder')}}">
                            </label>
                            <input class="search__button" type="submit" value="{{__('search.button_text')}}">
                        </div>
                    </form>
                </div>
                @if($items && count($items) > 0)
                    <div class="search__column">
                        @foreach($items as $item)
                            <a href="{{$item->url}}" class="search__entry">
                                <div class="search__group-description">
                                    <div class="search__name">{{$item->title}}</div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                @elseif($search_text)
                    <div class="search__message">
                        {{__('search.nothing_found_text')}}
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
