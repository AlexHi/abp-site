@extends('layouts.app')

@section('pageTitle', __('contacts.title'))

@section('ogTitle',  __('contacts.title'))
@section('ogImage', env('APP_URL'). '/storage/'. $site_info->passageway_image)
@section('ogType', 'contacts')
@section('ogUrl', Request::fullUrl())

@section('content')
    <section class="contact">
        <div class="container-wrap">
            <div class="contact__row">
                <h2 class="contact__title title">{{ __('contacts.title')}}</h2>
                <div class="contact__list">
                    <div class="contact__column">
                        <div class="contact__item">
                            <div class="contact__name">{{ __('contacts.address')}}</div>
                            <div class="contact__text">{{$site_info->address}}</div>
                        </div>
                        <div class="contact__item">
                            <div class="contact__name">{{ __('contacts.phone')}}</div>
                            <div class="contact__text"><a
                                    href="tel:{{$site_info->phone_number}}">{{$site_info->phone_number}}</a></div>
                        </div>
                        <div class="contact__item">
                            <div class="contact__name">{{ __('contacts.email')}}</div>
                            <div class="contact__text"><a href="mailto:{{$site_info->email}}">{{$site_info->email}}</a>
                            </div>
                        </div>
                        <div class="contact__item">
                            <div class="contact__name">{{ __('contacts.work_time')}}</div>
                            <div class="contact__text">{{$site_info->work_time}}</div>
                        </div>
                        @if($site_info->contactDocument)
                            <div class="contact__requisites">
                                <div class="contact__name-2">{{$site_info->contactDocument->name}}</div>
                                <div class="contact__download"><a href="/storage/{{$site_info->contactDocument->file}}"
                                                                  download="">{{ __('contacts.download_pdf')}}</a>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="contact__column">
                        @if($contact_user)
                            <div class="contact__text">
                                {!! __('contacts.user_text') !!}
                            </div>
                            <div class="contact__group">
                                <div class="contact__image">
                                    <img src="/storage/{{$contact_user->profile->image}}"
                                         alt="{{$contact_user->fullName()}}"/>
                                </div>
                                <div class="contact__group-2">
                                    <div class="contact__name-2">{{$contact_user->fullName()}}</div>
                                    <div class="contact__name">
                                        {{$contact_user->profile->work_position}}
                                    </div>
                                    <div class="contact__name">
                                        {{$contact_user->profile->extra_description}}
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="contact__row">
                <div class="contact__column">
                    <div class="contact__map">
                        <div id="map" style="width: 100%; height: 100%"></div>
                    </div>
                    <div class="contact__text">{{$site_info->driveway_title}}</div>
                    <div class="contact__name">
                        {{$site_info->driveway_description}}
                    </div>
                </div>
                <div class="contact__column">
                    <div class="contact__image-2">
                        <img src="/storage/{{$site_info->passageway_image}}" alt=""/>
                    </div>
                    <div class="contact__text">{{$site_info->passageway_title}}</div>
                    <div class="contact__name">
                        {{$site_info->passageway_description}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('components.form', ['form_title' => __('forms.ask_an_expert_title'), 'form_name' => 'expert_questions'])

    <script src="https://api-maps.yandex.ru/2.1/?apikey=7c03af3a-2e55-48da-972d-a1698f57b2f0&lang=ru_RU"
            type="text/javascript"></script>

    <script>
        const renderMap = function ({latitude, longitude, logo}) {
            ymaps.ready(function () {
                let myMap               = new ymaps.Map(
                    "map",
                    {
                        center:   [latitude, longitude],
                        zoom:     18,
                        controls: []
                    },
                    {
                        searchControlProvider: "yandex#search"
                    }
                    ),
                    // Создаём макет содержимого.
                    MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                        '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
                    ),
                    myPlacemark         = new ymaps.Placemark(
                        myMap.getCenter(),
                        {
                            hintContent:    "",
                            balloonContent: ""
                        },
                        {
                            // Опции.
                            // Необходимо указать данный тип макета.
                            iconLayout:      "default#image",
                            // Своё изображение иконки метки.
                            iconImageHref:   "/storage/" + logo,
                            // Размеры метки.
                            iconImageSize:   [90, 96],
                            // Смещение левого верхнего угла иконки относительно
                            // её "ножки" (точки привязки).
                            iconImageOffset: [0, -110]
                        }
                    );
                myMap.geoObjects.add(myPlacemark);
                myMap.behaviors.disable("scrollZoom");
            });
        };

        const map = {
            latitude: @json($site_info->map_latitude),
            longitude: @json($site_info->map_longitude),
            logo: @json($site_info->map_logo)
        };

        renderMap(map);
    </script>
@endsection
