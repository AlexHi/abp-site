@extends('layouts.app')

@section('pageTitle', $post->title)

@section('ogTitle', $post->title)
@section('ogDescription', $post->description)
@section('ogImage', env('APP_URL').'/storage/'.$post->image)
@section('ogType', 'news')
@section('ogUrl', Request::fullUrl())

@section('content')
    <section class="article">
        <div class="container-wrap">
            <div class="article__row">
                <h2 class="article__title title">{{$post->title}}</h2>
                <div class="article__column">
                    <div class="article__left">
                        <div class="article__image"><img src="/storage/{{$post->image}}" alt="{{$post->title}}"/></div>
                        <div class="article__text">
                            {!! $post->text !!}
                        </div>
                    </div>
                    <div class="article__right">
                        <div class="article__group">
                            @if($post->user)
                                <a href="/team/{{$post->user->id}}">
                                    <div class="article__team team-article">
                                        <div class="team-article__img"><img
                                                src="/storage/{{$post->user->profile->image}}"
                                                alt=""/></div>
                                        <div class="team-article__group-person">
                                            <div
                                                class="team-article__name">{{$post->user->name}} {{$post->user->last_name}}</div>
                                            <div
                                                class="team-article__post">{{$post->user->profile->work_position}}</div>
                                            <div class="team-article__email">{{$post->user->email}}</div>
                                        </div>
                                    </div>
                                </a>
                            @endif
                        </div>
                        <div class="article__group">
                            @if($post->source_link)
                                <div class="article__source">{{__('news.source')}}</div>
                                <div class="article__source-image">
                                    <a href="{{$post->source_link}}" target="_blank" rel="noreferrer">
                                        @if($post->source_image)
                                            <img src="/storage/{{$post->source_image}}" alt="{{$post->source_link}}">
                                        @else
                                            {{$post->source_link}}
                                        @endif
                                    </a>
                                </div>
                            @endif
                            <div class="article__date">{{$post->created_at->formatLocalized('%d.%m.%Y')}}</div>

                            <div class="article__share">
                                <script src="https://yastatic.net/share2/share.js"></script>
                                <div class="ya-share2" data-curtain data-shape="round" data-color-scheme="whiteblack"
                                     data-lang="{{env('APP_LOCALE')}}" data-limit="0" data-more-button-type="long"
                                     data-services="vkontakte,facebook,odnoklassniki,telegram,twitter,viber,whatsapp,skype,linkedin"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="article__row">
                <a href="/news" class="article__back"><span>{{__("news.back_link")}}</span></a>
                <div class="article__column">
                    @if(isset($posts))
                        @foreach($posts as $post)
                            <a class="article__wrap" target="_blank" href="/news/{{$post->id}}">
                                <div class="publication-1">
                                    <div class="publication-1__group">
                                        <div class="publication-1__date">
                                            {{$post->created_at->formatLocalized('%d.%m.%Y')}}
                                        </div>
                                        <div class="publication-1__name">{{$post->title}}</div>
                                    </div>
                                    <img class="publication-1__image"
                                         src="/storage/{{$post->image}}"
                                         alt="{{$post->title}}"/>
                                </div>
                            </a>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
