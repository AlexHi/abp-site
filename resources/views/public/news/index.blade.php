@extends('layouts.app')

@php
    $pageString = (PaginateRoute::currentPage() > 1) ? ' ' . PaginateRoute::currentPage() : '';
@endphp

@section('pageTitle', __('news.title') . $pageString)

@section('content')
    <section class="news">
        <div class="container-wrap">
            <div class="news__row">
                <h2 class="news__title title">{{__('news.title_index_h1')}}</h2>
                <div class="news__column">
                    @foreach($posts as $post)
                        <div class="news__entry entry anim-items anim-no-scrollTop">
                            <div class="entry__item">
                                <div class="entry__image">
                                    <a href="/news/{{$post->id}}">
                                        <img src="/storage/{{$post->image}}" alt=""/>
                                    </a>
                                </div>
                                <div class="entry__group">
                                    <div
                                        class="entry__data">{{$post->created_at->formatLocalized('%d.%m.%Y')}}</div>
                                    <div class="entry__name"><a href="/news/{{$post->id}}">{{$post->title}}</a></div>
                                    <div class="entry__discription">
                                        {{$post->description}}
                                    </div>
                                    <div class="entry__btn">
                                        <a href="/news/{{$post->id}}">
                                            <span>{{__('news.more_details')}}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="news__row">
                {{$posts->links('components.pagination')}}
            </div>
        </div>
    </section>
@endsection
