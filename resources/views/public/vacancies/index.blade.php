@extends('layouts.app')

@section('pageTitle', __('vacancies.title'))

@section('content')
    <section class="vacancy">
        <div class="container-wrap">
            <h2 class="vacancy__title title">{{__('vacancies.title_index_h1')}}</h2>
            <div class="vacancy__row-1">
                <div class="vacancy__column">
                    <div class="vacancy__name">{{__('vacancies.specialist')}}</div>
                    <div class="vacancy__text">
                        {{__('vacancies.specialist_text')}}
                    </div>
                    <div class="vacancy__text-2">{{__('vacancies.specialist_item_1')}}</div>
                    <div class="vacancy__text-2">{{__('vacancies.specialist_item_2')}}</div>
                </div>
                <div class="vacancy__column">
                    <div class="vacancy__name">{{__('vacancies.students')}}</div>
                    <div class="vacancy__text">
                        {{__('vacancies.students_text')}}
                    </div>
                </div>
            </div>
            <div class="vacancy__row-2">
                <div class="vacancy__discription">
                    {{__('vacancies.description')}} <a href="mailto:abp@abp.legal">abp@abp.legal</a>
                </div>
            </div>
        </div>
    </section>
@endsection
