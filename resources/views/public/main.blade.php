@extends('layouts.app')

@section('content')
    <section class="juridical">
        <div class="container-wrap">
            <div class="juridical__row">
                <h2 class="juridical__title title">
                    {!! __('main.main_block_juridical') !!}
                </h2>
                <div class="juridical__column">
                    <div class="juridical__item">{{ __('main.main_block_item_1') }}</div>
                    <div class="juridical__item">{{ __('main.main_block_item_2') }}</div>
                    <div class="juridical__item">{{ __('main.main_block_item_3') }}</div>
                    <div class="juridical__item">{{ __('main.main_block_item_4') }}</div>
                </div>

                <div class="media-d-flex juridical__phone_wrapper">

                    @include('components.phone-form', ['form_name' => 'phone'] )

                    <div>
                        <div class="juridical__promo media-d-flex">
                            <div class="juridical__text">
                                <a href="https://300.pravo.ru/award/search/?AwardSearch%5Bquery%5D=Афонин%2C+божор"
                                   target="_blank" rel="noopener">
                                    <span style="color: #000000; ">{{ __('main.main_block_promo') }}</span>
                                </a>
                            </div>
                            <div class="juridical__logos">
                                <a class="juridical__logos__kommersant" href="https://www.kommersant.ru/doc/5913860"
                                   target="_blank" rel="noopener">
                                    <img src="/images/Logo_Kommersant.svg" alt="Коммерсант"/>
                                </a>
                                <a class="juridical__logos__pravo"
                                   href="https://300.pravo.ru/award/search/?AwardSearch%5Bquery%5D=Афонин%2C+божор"
                                   target="_blank" rel="noopener">
                                    <img src="/images/juridical-promo-image.png" alt="Право 300"/>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="trust">
        <div class="container-wrap">
            <div class="trust__row">
                <h2 class="trust__title title">{{ __('main.trust_title') }}</h2>
                <div class="trust__column">
                    <div class="swiper-wrapper">
                        <div class="trust__wrap swiper-slide">
                            <img class="trust__image" src="/storage/{{$site_info->trust_clients_image}}"
                                 alt="partners"/>
                        </div>
                        <div class="trust__wrap swiper-slide">
                            <img class="trust__image" src="/storage/{{$site_info->trust_clients_image}}"
                                 alt="partners"/>
                        </div>
                        <div class="trust__wrap swiper-slide">
                            <img class="trust__image" src="/storage/{{$site_info->trust_clients_image}}"
                                 alt="partners"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    @include('public.main.components.services', $servicesDirections)

    @include('public.main.components.publish', $publish)

    @include('public.main.components.reviews', $reviews)

    @include('public.main.components.media', $expert_links)


    @include('components.form', ['form_name' => 'questions'])

@endsection
