@extends('layouts.app')

@section('pageTitle', __('projects.title'))

@section('content')
    <section class="projects">
        <div class="container-wrap">
            <div class="projects_row">
                <h2 class="projects__title title">{{__('projects.title_index_h1')}}</h2>

                <div class="support__tabs tabs-support">
                    <div class="tabs-support__controls">
                        @foreach($project_categories as $category)
                            <a href="#tabs-support-{{$category->id}}" class="tabs-support__button tab-btn">
                                <img src="/storage/{{$category->icon}}" alt="{{$category->name}}"/>
                                <span>{{$category->name}}</span>
                            </a>
                        @endforeach
                    </div>
                    <div class="tabs-support__content">
                        @foreach($project_categories as $category)
                            <div id="tabs-support-{{$category->id}}" class="tabs-support__item tab-item">
                            <div class="projects__column">
                                @foreach($category->projects as $project)
                                    <a href="/projects/{{$project->id}}" class="projects__entry">
                                        <div class="projects__group projects__group_firm">
                                            <div class="projects__logo"><img src="/storage/{{$project->client->logo}}"
                                                                             alt="{{$project->client->name}}"
                                                                             class="mCS_img_loaded"></div>
                                            <div class="projects__name-firm">{{$project->client->name}}</div>
                                            <div class="projects__button"><span>{{__('projects.more_details')}}</span></div>
                                        </div>
                                        <div class="projects__group projects__group_discription">
                                            <div class="projects__name">{{$project->title}}</div>
                                            <div class="projects__discription">
                                                {{$project->description}}
                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                                </div>
                                <div class="data-container"></div>
                                <div class="pagination-container"></div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
