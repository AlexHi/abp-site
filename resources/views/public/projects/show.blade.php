@extends('layouts.app')

@section('pageTitle', $project->title)

@section('ogTitle', $project->title)
@section('ogDescription', $project->description)
@section('ogImage', env('APP_URL').'/storage/'. $project->client->logo)
@section('ogType', 'project')
@section('ogUrl', Request::fullUrl())

@section('content')
    <section class="article art-projects">
        <div class="container-wrap">
            <div class="art-projects__row">
                <h2 class="art-projects__title title">{{$project->title}}</h2>

                <div class="art-projects__column art-projects__column_one">

                    <div class="art-projects__group">
                        <div class="art-projects__client">{{__('projects.client')}}</div>

                        <div class="art-projects__group-info">

                            <div class="art-projects__group-logo">
                                <div class="art-projects__firm">{{$project->client->name}}</div>
                                <div class="art-projects__logo">
                                    <img src="/storage/{{$project->client->logo}}" alt="{{$project->client->name}}"/>
                                </div>
                            </div>

                            <div class="art-projects__text">
                                {{$project->client->description}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="art-projects__row">
                <div class="art-projects__column art-projects__column_two">
                    <div class="art-projects__left">
                        <div class="article__text">{!! $project->text !!}</div>

                    </div>
                    <div class="art-projects__right">
                        <div class="article__group">
                            <a href="/team/{{$project->user->id}}">
                                <div class="article__team team-article">
                                    <div class="team-article__img"><img
                                            src="/storage/{{$project->user->profile->image}}"
                                            alt="{{$project->user->fullName()}}"/></div>
                                    <div class="team-article__group-person">
                                        <div class="team-article__name">{{$project->user->fullName()}}</div>
                                        <div class="team-article__post">{{$project->user->profile->work_position}}</div>
                                        <div class="team-article__email">{{$project->user->email}}</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <a href="/projects" class="article__back"><span>{{__('projects.back_link')}}</span></a>
        </div>
    </section>
@endsection
