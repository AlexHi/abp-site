<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @include('favicons')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if($user)
        <script src="{{ mix('js/app.js') }}" defer></script>
        <link href="{{ asset('css/vuetify-components.css') }}" rel="stylesheet">
    @endif
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <title>
        @hasSection('pageTitle')
            @yield('pageTitle') | {{config('app.name', 'Афонин, Божор и партнёры')}}
        @else
            {{config('app.name', 'Афонин, Божор и партнёры')}}

        @endif
    </title>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous" defer></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="{{ asset('js/slick.min.js') }}" defer></script>
    <script src="{{ asset('js/jquery.mCustomScrollbar.js') }}" defer></script>
    <script src="https://yastatic.net/share2/share.js"></script>
    <script src="{{ asset('js/pagination.js') }}" defer></script>
    <script src="{{ asset('js/script.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ mix('css/style.min.css') }}" rel="stylesheet">

    @if($site_info)
        <meta name="description" content="{{$site_info->meta_description}}">
        <meta name="keywords" content="{{$site_info->meta_keywords}}"/>
        {!! $site_info->script_head !!}
    @endif

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        function onSubmit(token) {
            document.getElementById("form-request").submit();
        }
    </script>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                       new Date().getTime(),
                event: 'gtm.js'
            });
            var f  = d.getElementsByTagName(s)[0],
                j  = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TLC3GG4');</script>
    <!-- End Google Tag Manager -->

    <meta name="facebook-domain-verification" content="wey5m0n4xs9xgiee4trkwixsekrnz9"/>
    @hasSection('ogTitle')
        <meta property="og:title" content="@yield('ogTitle')"/>
    @endif
    @hasSection('ogDescription')
        <meta property="og:description" content="@yield('ogDescription')"/>
    @endif
    @hasSection('ogImage')
        <meta property="og:image" content="@yield('ogImage')"/>
    @endif
    @hasSection('ogType')
        <meta property="og:type" content="@yield('ogType')"/>
    @endif
    @hasSection('ogUrl')
        <meta property="og:url" content="@yield('ogUrl')"/>
    @endif
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            i;
            f(f.fbq);
            {
                return;
            }
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments);
            };
            if
            (!f._fbq) {
                f._fbq = n;
            }
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s);
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '216516743666957');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=216516743666957&ev=PageView&noscript=1
    "
        /></noscript>
    <!-- End Facebook Pixel Code -->

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TLC3GG4"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
@if($site_info)
    {!! $site_info->script_before_body !!}
@endif
<?
$keys = array('utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term');
foreach ($keys as $row) {
    if (!empty($_GET[$row])) {
        $value = strval($_GET[$row]);
        $value = stripslashes($value);
        $value = htmlspecialchars_decode($value, ENT_QUOTES);
        $value = strip_tags($value);
        $value = htmlspecialchars($value, ENT_QUOTES);
        setcookie('utm[' . $row . ']', $value);
    }
}

//echo '<pre>'; print_r($_COOKIE); echo '</pre>';?>
<div id="app">
    <div class="wrapper">
        @if($user)
            <site-admin-panel :user='@json($user)' :site-info='@json($site_info)'></site-admin-panel>
        @endif

        <header class="header">
            <div class="container-wrap">
                <div class="header__row">
                    <div class="header__logo">
                        <a href="/">
                            <img
                                src="/storage/{{$site_info->header_logo}}"
                                alt="logo"/>
                        </a>
                    </div>
                    <nav class="header__nav">
                        <ul class="header__list">
                            <li class="header__item">
                                <a class="{{ request()->is('/') ? 'active' : '' }}" href="/">
                                    {{__('menus.main')}}
                                </a>
                            </li>
                            <li class="header__item">
                                <a class="{{ request()->is('team*') ? 'active' : '' }}"
                                   href="/team">
                                    {{__('menus.team')}}
                                </a>
                            </li>
                            <li class="header__item">
                                <a class="{{ request()->is('services*') ? 'active' : '' }}"
                                   href="/services">
                                    {{__('menus.services')}}
                                </a>
                            </li>
                            <li class="header__item">
                                <a class="{{ request()->is('news*') ? 'active' : '' }}"
                                   href="/news">
                                    {{__('menus.news')}}
                                </a>
                            </li>
                            <li class="header__item">
                                <a class="{{ request()->is('articles*') ? 'active' : '' }}"
                                   href="/articles">
                                    {{__('menus.articles')}}
                                </a>
                            </li>
                            <li class="header__item">
                                <a class="{{ request()->is('projects*') ? 'active' : '' }}"
                                   href="/projects">
                                    {{__('menus.projects')}}
                                </a>
                            </li>
                            <li class="header__item">
                                <a class="{{ request()->is('contacts*') ? 'active' : '' }}"
                                   href="/contacts">
                                    {{__('menus.contacts')}}
                                </a>
                            </li>
                        </ul>
                    </nav>

                    <div class="header__mobile mobile-menu">
                        <div class="container-wrap">
                            <div class="mobile-menu__wrap">
                                <div class="mobile-menu__group mobile-menu__group_link">
                                    <div class="mobile-menu__item">
                                        <a class="{{ request()->is('/') ? 'active' : '' }}"
                                           href="/">
                                            {{__('menus.main')}}
                                        </a>
                                    </div>
                                    <div class="mobile-menu__item"><a
                                            class="{{ request()->is('team*') ? 'active' : '' }}"
                                            href="/team">
                                            {{__('menus.team')}}
                                        </a>
                                    </div>
                                    <div class="mobile-menu__item">
                                        <a class="{{ request()->is('services*') ? 'active' : '' }}" href="/services">
                                            {{__('menus.services')}}
                                        </a>
                                    </div>
                                    <div class="mobile-menu__item">
                                        <a class="{{ request()->is('news*') ? 'active' : '' }}"
                                           href="/news">
                                            {{__('menus.news')}}
                                        </a>
                                    </div>
                                    <div class="mobile-menu__item">
                                        <a class="{{ request()->is('articles*') ? 'active' : '' }}" href="/articles">
                                            {{__('menus.articles')}}
                                        </a>
                                    </div>
                                    <div class="mobile-menu__item">
                                        <a class="{{ request()->is('projects*') ? 'active' : '' }}" href="/projects">
                                            {{__('menus.projects')}}
                                        </a>
                                    </div>
                                    <div class="mobile-menu__item">
                                        <a class="{{ request()->is('contacts*') ? 'active' : '' }}" href="/contacts">
                                            {{__('menus.contacts')}}
                                        </a>
                                    </div>
                                </div>

                                <div class="mobile-menu__group mobile-menu__group_language">
                                    <div class="mobile-menu__lang-item">
                                        {{__('main.site_version')}}
                                        <span>
                                            @if(env('APP_LOCALE') === 'ru')
                                                <a class="active">ru</a>
                                                <a href="{{env('APP_MIRROR_URL')}}">eng</a>
                                            @else
                                                <a href="{{env('APP_MIRROR_URL')}}">ru</a>
                                                <a class="active">eng</a>
                                            @endif
                                        </span>
                                    </div>
                                </div>

                                <div class="mobile-menu__group mobile-menu__group_info">
                                    <div
                                        class="mobile-menu__text mobile-menu__text_addres">{{$site_info->address}}</div>
                                    <div class="mobile-menu__text mobile-menu__text_time-work">
                                        {{ __('contacts.work_time') }}  {{$site_info->work_time}}</div>
                                </div>

                                <div class="mobile-menu__group mobile-menu__group_contact">
                                    <div class="mobile-menu__text mobile-menu__text_phone"><a
                                            href="tel:{{$site_info->phone_number}}">{{$site_info->phone_number}}</a>
                                    </div>
                                    <div class="mobile-menu__text mobile-menu__text_email"><a
                                            href="mailto:{{$site_info->email}}">{{$site_info->email}}</a></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="header__info">
                        <div class="header__phone"><a
                                href="tel:{{$site_info->phone_number}}">{{$site_info->phone_number}}</a></div>
                        <div class="header__lang">
                            <a href="{{env('APP_MIRROR_URL')}}">
                                {{env('APP_LOCALE') === 'ru' ? 'ENG' : 'RU'}}
                            </a>
                        </div>
                        <a class="header__search" href="/search"></a>
                        <div class="header__burger"><span></span></div>
                    </div>
                </div>
            </div>
        </header>
        @yield('content')
        <footer class="footer">
            <div class="container-wrap">
                <div class="footer__row">
                    <div class="footer__column">
                        <div class="footer__logo">
                            <a href="/">
                                <img src="/storage/{{$site_info->footer_logo}}"
                                     alt="{{config('app.name', 'Афонин, Божор и партнёры')}}"/>
                            </a>
                        </div>
                        <div class="footer__social">
                            @if($site_info && $site_info->socialMedias)
                                @foreach($site_info->socialMedias as $socialMedia)
                                    <a href="{{$socialMedia->link}}" target="_blank" rel="noreferrer">
                                        <img width="30px" height="30px" src="/storage/{{$socialMedia->logo}}"
                                             alt="{{$socialMedia->name}}"/>
                                    </a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="footer__column">
                        <div class="footer__info">
                            <div class="footer__name">{{__('menus.about.company')}}</div>
                            <ul class="footer__list">
                                <li class="footer__item"><a href="/">{{__('menus.main')}}</a></li>
                                <li class="footer__item"><a href="/team">{{__('menus.team')}}</a></li>
                                <li class="footer__item"><a href="/projects">{{__('menus.projects')}}</a></li>
                                <li class="footer__item"><a href="/articles">{{__('menus.articles')}}</a></li>
                                <li class="footer__item"><a href="/services">{{__('menus.services')}}</a></li>
                                <li class="footer__item"><a href="/vacancies">{{__('menus.vacancies')}}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="footer__column">
                        <div class="footer__addition">
                            <div class="footer__name">{{__('menus.additional')}}</div>
                            <ul class="footer__list">
                                <li class="footer__item"><a href="/news">{{__('menus.news')}}</a></li>
                                <li class="footer__item"><a href="/clients">{{__('menus.clients')}}</a></li>
                                <li class="footer__item"><a href="/videos">{{__('menus.videos')}}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="footer__column">
                        <div class="footer__contact">
                            <div class="footer__name">{{ __('contacts.title') }}</div>
                            <ul class="footer__list">
                                <li class="footer__item"><a
                                        href="tel:{{$site_info->phone_number}}">{{$site_info->phone_number}}</a></li>
                                <li class="footer__item"><a
                                        href="mailto:{{$site_info->email}}">{{$site_info->email}}</a></li>
                                <li class="footer__item">{{$site_info->address}}</li>
                                <li class="footer__item">{{ __('contacts.work_time') }} {{$site_info->work_time}}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="footer__column">
                        <div class="footer__subscribe">
                            <div class="footer__name">Подписаться на рассылку</div>
                            <form action="" class="footer__form">
                                <input type="email" name="email" placeholder="Ваш e-mail"/>
                                <button class="footer__button">
                                    <span>Подписаться</span>
                                </button>
                                <label class="footer__sub checkbox-form">
                                    Я согласен на
                                    @if($site_info->pdppDocument)
                                        <a href="/storage/{{$site_info->pdppDocument->file}}" download>обработку
                                            персональных данных</a>
                                    @else
                                        обработку персональных данных
                                    @endif
                                    <input type="checkbox">
                                    <span class="checkbox-form__checkmark"></span>
                                </label>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer__row">
                    <div class="footer__group">
                        @if($site_info->pdppDocument)
                            <div class="footer__poilicy">
                                <a href="/storage/{{$site_info->pdppDocument->file}}" download>
                                    {{$site_info->pdppDocument->name}}
                                </a>
                            </div>
                        @endif
                        @if($site_info->eulaDocument)
                            <div class="footer__poilicy">
                                <a href="/storage/{{$site_info->eulaDocument->file}}" download>
                                    {{$site_info->eulaDocument->name}}
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="st
eet">
@if($site_info)
    {!! $site_info->script_after_body !!}
@endif
</body>
</html>
