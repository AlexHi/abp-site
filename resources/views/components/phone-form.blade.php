@if(isset($form_name))
    <section id="form" class="phone-form">
        <div class="message__row">
            <h3>
                @if(isset($form_title))
                    {!! $form_title !!}
                @else
                    {!! __('forms.order_consultation') !!}
                @endif
            </h3>
            <div class="message__column">
                <form action="/form/{{$form_name}}#form" id="form-request" class="message__form form"
                      enctype="multipart/form-data" method="post">
                    @csrf
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                    <div class="form__phone-group">
                        @error('phone_number')
                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('phone_number') }}</strong>
                                        </span>
                        @enderror
                        <input class="form__phone" type="tel" name="phone_number"
                               autocomplete="tel"
                               value="{{old('phone_number')}}"
                               placeholder="{{ __('forms.phone_number_placeholder') }}"/>
                    </div>

                    <button class="g-recaptcha form__submit"
                            data-sitekey="{{env('RECAPTCHA_SITE_KEY')}}"
                            data-callback='onSubmit'>
                        {{ __('forms.button') }}
                    </button>

                </form>
            </div>
        </div>
    </section>
@endif
