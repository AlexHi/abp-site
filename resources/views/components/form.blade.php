@if(isset($form_name))
    <section class="message" id="form">
        <div class="container-wrap">
            <div class="message__row">
                <h2 class="message__title title">
                    @if(isset($form_title))
                        {!! $form_title !!}
                    @else
                        {!! __('forms.default_title') !!}
                    @endif
                </h2>
                <div class="message__column">
                    <form action="/form/{{$form_name}}#form" id="form-request" class="message__form form"
                          enctype="multipart/form-data" method="post">
                        @csrf
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        <div class="form__group">
                            <div class="form__name-group">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @enderror
                                <input class="form__name" type="text" name="name" value="{{old('name')}}"
                                       placeholder="{{ __('forms.name_placeholder') }}"/>
                            </div>

                            <div class="form__email-group">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                @enderror
                                <input class="form__email" type="email" name="email" value="{{old('email')}}"
                                       placeholder="{{ __('forms.email_placeholder') }}"/>
                            </div>

                        </div>

                        <div class="form__phone-group">
                            @error('phone_number')
                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('phone_number') }}</strong>
                                        </span>
                            @enderror
                            <input class="form__phone" type="tel" name="phone_number"
                                   autocomplete="tel"
                                   value="{{old('phone_number')}}"
                                   placeholder="{{ __('forms.phone_number_placeholder') }}"/>
                        </div>

                        <div class="form__message-group">
                            @error('message')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                            @enderror
                            <textarea class="form__letter" name="message"
                                      placeholder="{{ __('forms.message_placeholder') }}">{{old('message')}}</textarea>
                        </div>

                        <label class="form__sub checkbox-form">
                            @error('agreement')
                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('agreement') }}</strong>
                                        </span>
                            @enderror
                            {{ __('forms.agreement') }}
                            @if($site_info->pdppDocument)
                                <a href="/storage/{{$site_info->pdppDocument->file}}" download>
                                    {{ __('forms.agreement_file') }}
                                </a>
                            @else
                                {{ __('forms.agreement_file') }}
                            @endif
                            <input type="checkbox" name="agreement">
                            <span class="checkbox-form__checkmark"></span>
                        </label>

                        <button class="g-recaptcha form__submit"
                                data-sitekey="{{env('RECAPTCHA_SITE_KEY')}}"
                                data-callback='onSubmit'>
                            {{ __('forms.button') }}
                        </button>

                    </form>
                </div>
            </div>
        </div>
    </section>
@endif
