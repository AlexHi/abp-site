@if ($paginator->hasPages())
    <nav class="pagination">
        <ul>
            <li>
                @if(PaginateRoute::hasPreviousPage())
                    <a href="{{PaginateRoute::previousPageUrl()}}"
                       class="pagination__page-num prev">
                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="22" viewBox="0 0 13 22"
                             fill="none">
                            <path d="M12 1L2 11L12 21" stroke="white" stroke-width="2"/>
                        </svg>
                    </a>
                @else
                    <span class="pagination__page-num prev inactively">
                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="22" viewBox="0 0 13 22"
                             fill="none">
                            <path d="M12 1L2 11L12 21" stroke="white" stroke-width="2"/>
                        </svg>
                    </span>
                @endif
            </li>

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li><span aria-disabled="true" class="pagination__page-num">...</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        <li>
                            <a class="pagination__page-num {{$page == $paginator->currentPage() ? 'current' : ''}}"
                               href="{{ PaginateRoute::pageUrl($page) }}">
                                {{$page}}
                            </a>
                        </li>
                    @endforeach
                @endif
            @endforeach

            <li>
                @if(PaginateRoute::hasNextPage($paginator))
                    <a href="{{ PaginateRoute::nextPageUrl($paginator) }}" class="pagination__page-num next">
                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="22" viewBox="0 0 13 22" fill="none">
                            <path d="M12 1L2 11L12 21" stroke="white" stroke-width="2"/>
                        </svg>
                    </a>
                @else
                    <span class="pagination__page-num next inactively">
                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="22" viewBox="0 0 13 22" fill="none">
                            <path d="M12 1L2 11L12 21" stroke="white" stroke-width="2"/>
                        </svg>
                    </span>
                @endif
            </li>
        </ul>
    </nav>
@endif
