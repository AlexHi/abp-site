<table style="background-color: #f5faff; width: 100%; height:100%; font-family: Arial, sans-serif;">
    <tr>
        <td></td>
        <td style="display: block !important; max-width: 800px !important; margin: 0 auto !important; clear: both !important;">
            <div style="max-width: 800px; margin: 0 auto; display: block; padding: 20px 0;">
                <table style="width: 100%; background: #fff; border: 2px solid #0a2783; border-spacing: 0;">
                    <tr>
                        <td style="font-size: 16px; color: #fff; font-weight: 500; padding: 20px; text-align: center; background: #0a2783;">
                            {{$subject}}
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 20px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="padding: 0 0 20px;">
                                        Добрый день!
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding: 0 0 20px;">
                                        Заполнена новая заявка на сайте <a href="{{$site_url}}">{{$site_url}}</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding: 0 0 20px;">
                                        <strong>Имя:</strong> {{$name}}
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding: 0 0 20px;">
                                        <strong>E-mail:</strong> {{$email}}
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding: 0 0 20px;">
                                        <strong>Телефон:</strong> {{$phone_number}}
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding: 0 0 20px;">
                                        <strong>Вопрос к:</strong> <a href="{{$thing_url}}">{{$thing_name}}</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding: 0 0 10px;">
                                        <strong>Сообщение:</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:30px; background: #fff;border:1px solid #ebebeb; width: 100%;">
                                        {{$user_message}}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 10px 0 10px;">
                                        <strong>UTM:</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:30px; background: #fff;border:1px solid #ebebeb; width: 100%;">
                                        {{$utm}}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 20px 0 20px; text-align: center;">
                                        <a href="{{$site_url}}admin/form-requests/{{$id}}"
                                           style="text-decoration: none; color: #FFF; background-color: #0a2783; padding: 5px 15px; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block;"
                                           target="_blank">Посмотреть в панели управления</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td></td>
    </tr>
</table>
