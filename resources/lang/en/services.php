<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Services Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on index and detail pages of services.
    |
    */
    'title'                  => 'Services',
    'title_index_h1'         => 'Our prices and services',
    'subtitle_index'         => 'Services:',
    'service_price'          => 'Service price',
    'total'                  => 'Total',
    'service_request_button' => 'Leave a request',
    'back_link'              => 'Back to the list of services',
    'term_of_the_work'       => 'Term of the work',
    'form_title'             => 'Order service',
];
