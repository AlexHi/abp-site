<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in every forms on site.
    |
    */
    'default_title'            => 'Should you have any questions <br/> Please do not hesitate to contact us',
    'ask_an_expert_title'      => 'Ask an expert',
    'name_placeholder'         => 'Your name',
    'email_placeholder'        => 'Your e-mail',
    'phone_number_placeholder' => 'Your phone number',
    'message_placeholder'      => 'Your message',
    'question_placeholder'     => 'Your question',
    'agreement'                => 'I accept',
    'agreement_file'           => 'the rules for the processing of personal data',
    'button'                   => 'Send',
    'order_consultation'       => 'Order a consultation now'
];
