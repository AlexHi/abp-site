<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main Page Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on main page.
    |
    */
    'main_block_juridical'    => 'Legal services in Russia for <br> <span>IT</span> and <span>E-commerce companies </span>',
    'main_block_item_1'       => 'Development of user agreements',
    'main_block_item_2'       => 'Drafting contracts with software developers',
    'main_block_item_3'       => 'Preparation of license agreements, transfer of IP',
    'main_block_item_4'       => 'Development of personal data protection documents',
    'main_block_promo'        => 'In 2021 we were among the best Russian law firms in the nominations Digital Economy, TMT and Copyright',
    'trust_title'             => 'We are trusted by more than 500+ clients:',
    'services_title'          => 'Services',
    'services_button'         => 'All services',
    'publish_title'           => 'We share professional experience and publish in the media',
    'publish_news_title'      => 'News',
    'publish_videos_title'    => 'Videos',
    'publish_articles_title'  => 'Articles',
    'publish_news_button'     => 'All news',
    'publish_videos_button'   => 'All videos',
    'publish_articles_button' => 'All articles',
    'reviews_title'           => 'Feedback from our clients',
    'site_version'            => 'Site version',
    'media_title'             => 'We are experts in the media',
];
