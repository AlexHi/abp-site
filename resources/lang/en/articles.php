<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Articles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on index and detail pages of articles.
    |
    */
    'title'          => 'Analytics and articles',
    'title_index_h1' => 'Analytics and articles',
    'source'         => 'Source:',
    'back_link'      => 'Back to the list of articles',
];
