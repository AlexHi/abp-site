<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Videos Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on index and detail pages of videos.
    |
    */
    'title'          => 'Videos',
    'title_index_h1' => 'Videos',
    'back_link'      => 'Back to the video list',
];
