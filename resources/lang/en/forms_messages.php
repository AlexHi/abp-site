<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in forms controller.
    |
    */
    'name_required'      => 'Required field',
    'email_required'     => 'Required field',
    'phone_required'     => 'Required field',
    'phone_format'       => 'Invalid phone number format',
    'message_required'   => 'You must fill in the field',
    'agreement_accepted' => 'You must accept the rules for the processing of personal data',
    'success'            => 'Thank you for contacting us!'
];
