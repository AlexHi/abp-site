<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in header and footer menus.
    |
    */
    'main'          => 'Main',
    'team'          => 'Team',
    'services'      => 'Services',
    'news'          => 'News',
    'articles'      => 'Analytics',
    'projects'      => 'Projects',
    'contacts'      => 'Contacts',
    'vacancies'     => 'Vacancies',
    'about.company' => 'About company',
    'additional'    => 'Additional',
    'clients'       => 'Clients',
    'videos'        => 'Videos'
];
