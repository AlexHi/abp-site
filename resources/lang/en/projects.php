<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Projects Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on index and detail pages of projects.
    |
    */
    'title'          => 'Projects',
    'title_index_h1' => 'Projects and clients',
    'more_details'   => 'More details',
    'client'         => 'Client:',
    'back_link'      => 'Back to the list of projects',
];
