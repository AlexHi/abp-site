<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Search Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on search page.
    |
    */
    'title'              => 'Search',
    'title_h1'           => 'Site search',
    'button_text'        => 'Search',
    'input_placeholder'  => 'Enter your search term',
    'nothing_found_text' => 'We could not find anything for your request.'
];
