<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Vacancies Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on vacancies page.
    |
    */
    'title'             => 'Vacancies',
    'title_index_h1'    => 'Vacancies and internships',
    'specialist'        => 'For specialists',
    'specialist_text'   => 'Due to the constant development and business expansion, the company "Afonin Bozhor and Partners" is constantly considering bringing new employees on the job:',
    'specialist_item_1' => 'Patent and Trademark Attorneys',
    'specialist_item_2' => 'Assistant Patent Attorneys',
    'students'          => 'Law students',
    'students_text'     => 'Afonin, Bojor and Partners organizes internships for students throughout the academic year, and during the summer and winter student holidays. Duration of the internship - from 2 weeks to 1 month',
    'description'       => 'Are you interested? Send your resume with a short story about yourself to'
];
