<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Teams Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on index and detail pages of team.
    |
    */
    'title'             => 'Team',
    'our_team'          => 'Our Team',
    'ask_a_question'    => 'Ask a Question',
    'education'         => 'Education:',
    'language_skills'   => 'Language skills:',
    'contacts'          => 'Contacts:',
    'specialisation'    => 'Specialisation:',
    'projects'          => 'Expert projects',
    'projects_more'     => 'More details',
    'publications'      => 'Publications',
    'publications_more' => 'More details',
    'ask_an_expert'     => 'Ask an expert'
];

