<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contacts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in contacts page and related site elements.
    |
    */
    'title'        => 'Contacts',
    'address'      => 'Address:',
    'phone'        => 'Phone:',
    'email'        => 'E-mail:',
    'work_time'    => 'Working Hours:',
    'download_pdf' => 'Download as pdf',
    'user_text'    => 'You will be communicating with a lawyer,<br/> not with sales managers',
];
