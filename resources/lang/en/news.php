<?php

return [

    /*
    |--------------------------------------------------------------------------
    | News Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on index and detail pages of news.
    |
    */
    'title'          => 'News',
    'title_index_h1' => 'News',
    'more_details'   => 'More details',
    'source'         => 'Source:',
    'back_link'      => 'Back to news list',
];
