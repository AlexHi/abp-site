<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contacts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in contacts page and related site elements.
    |
     */
    'title'        => 'Контакты',
    'address'      => 'Адрес:',
    'phone'        => 'Телефон:',
    'email'        => 'E-mail:',
    'work_time'    => 'Время работы:',
    'download_pdf' => 'Скачать в формате pdf',
    'user_text'    => 'Вы будете общаться с юристом,<br/> а не с менеджерами по продажам',
];
