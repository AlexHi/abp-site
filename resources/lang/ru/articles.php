<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Articles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on index and detail pages of articles.
    |
    */
    'title'          => 'Аналитика и статьи',
    'title_index_h1' => 'Аналитика и статьи',
    'source'         => 'Источник:',
    'back_link'      => 'Возврат к списку статей',
];
