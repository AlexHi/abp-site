<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Services Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on index and detail pages of services.
    |
    */
    'title'                  => 'Услуги',
    'title_index_h1'         => 'Наши цены и услуги',
    'subtitle_index'         => 'Услуги:',
    'service_price'          => 'Цена за услуги',
    'total'                  => 'Итого',
    'service_request_button' => 'Оставить заявку',
    'back_link'              => 'Возврат к списку услуг',
    'term_of_the_work'       => 'Сроки работ',
    'form_title'             => 'Заказать услугу',
];
