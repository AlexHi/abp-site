<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Teams Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on index and detail pages of team.
    |
    */
    'title'             => 'Команда',
    'our_team'          => 'Наша команда',
    'ask_a_question'    => 'Задать вопрос',
    'education'         => 'Образование:',
    'language_skills'   => 'Владение языками:',
    'contacts'          => 'Контакты:',
    'specialisation'    => 'Специализация:',
    'projects'          => 'Проекты эксперта',
    'projects_more'     => 'Подробнее',
    'publications'      => 'Публикации',
    'publications_more' => 'Подробнее',
    'ask_an_expert'     => 'Задайте вопрос<br> эксперту'
];
