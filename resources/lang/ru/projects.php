<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Projects Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on index and detail pages of projects.
    |
    */
    'title'          => 'Проекты',
    'title_index_h1' => 'Проекты и клиенты',
    'more_details'   => 'Подробнее',
    'client'         => 'Клиент:',
    'back_link'      => 'Возврат к списку проектов',
];
