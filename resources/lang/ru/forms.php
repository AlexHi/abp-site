<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in every forms on site.
    |
    */
    'default_title'            => 'Остались вопросы?<br/> Мы с радостью на них ответим',
    'ask_an_expert_title'      => 'Задать вопрос эксперту',
    'name_placeholder'         => 'Ваше имя',
    'email_placeholder'        => 'Ваш e-mail',
    'phone_number_placeholder' => 'Ваш номер телефона',
    'message_placeholder'      => 'Ваше сообщение',
    'question_placeholder'     => 'Ваш вопрос',
    'agreement'                => 'Я принимаю',
    'agreement_file'           => 'правила обработки персональных данных',
    'button'                   => 'Отправить',
    'order_consultation'       => 'Закажи консультацию сейчас'
];
