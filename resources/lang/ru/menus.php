<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in header and footer menus.
    |
    */
    'main'          => 'Главная',
    'team'          => 'Команда',
    'services'      => 'Услуги',
    'news'          => 'Новости',
    'articles'      => 'Аналитика',
    'projects'      => 'Проекты',
    'contacts'      => 'Контакты',
    'vacancies'     => 'Вакансии',
    'about.company' => 'О компании',
    'additional'    => 'Дополнительно',
    'clients'       => 'Клиенты',
    'videos'        => 'Видео'
];
