<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Videos Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on index and detail pages of videos.
    |
    */
    'title'          => 'Видео',
    'title_index_h1' => 'Видео',
    'back_link'      => 'Возврат к списку видео',
];
