<?php

return [

    /*
    |--------------------------------------------------------------------------
    | News Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on index and detail pages of news.
    |
    */
    'title'          => 'Новости',
    'title_index_h1' => 'Новости',
    'more_details'   => 'Подробнее',
    'source'         => 'Источник:',
    'back_link'      => 'Возврат к списку новостей',
];
