<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Search Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on search page.
    |
    */
    'title'              => 'Поиск',
    'title_h1'           => 'Поиск по сайту',
    'button_text'        => 'Поиск',
    'input_placeholder'  => 'Введите поисковый запрос',
    'nothing_found_text' => 'Мы не смогли ничего найти по Вашему запросу.'
];
