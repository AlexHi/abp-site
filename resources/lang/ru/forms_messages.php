<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in forms controller.
    |
    */
    'name_required'      => 'Обязательное поле',
    'email_required'     => 'Обязательное поле',
    'phone_required'     => 'Обязательное поле',
    'phone_format'       => 'Неправильный формат номера телефона',
    'message_required'   => 'Необходимо заполнить поле',
    'agreement_accepted' => 'Необходимо принять правила обработки персональных данных',
    'success'            => 'Благодарим за обращение!'
];
