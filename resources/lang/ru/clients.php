<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Clients Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on index page of clients.
    |
    */
    'title'          => 'Клиенты',
    'title_index_h1' => 'Клиенты',
];
