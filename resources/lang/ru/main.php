<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main Page Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on main page.
    |
    */
    'main_block_juridical'    => 'Юридические услуги<br> в сфере <span>IT</span> и <span>E-commerce</span>',
    'main_block_item_1'       => 'Разработка пользовательских соглашений',
    'main_block_item_2'       => 'Оформление отношений с разработчиками ПО',
    'main_block_item_3'       => 'Лицензионные договоры и отчуждение прав на ПО',
    'main_block_item_4'       => 'Разработка документов по персональным данным',
    'main_block_promo'        => 'С 2020 года входим в число лучших юридических фирм в номинациях Цифровая экономика, ТМТ и Copyright',
    'trust_title'             => 'Нам доверяют:',
    'services_title'          => 'Оказываем услуги',
    'services_button'         => 'Все услуги',
    'publish_title'           => 'Делимся профессиональным опытом и публикуемся в СМИ',
    'publish_news_title'      => 'Новости',
    'publish_videos_title'    => 'Видео',
    'publish_articles_title'  => 'Публикации',
    'publish_news_button'     => 'Все новости',
    'publish_videos_button'   => 'Все видео',
    'publish_articles_button' => 'Все публикации',
    'reviews_title'           => 'Отзывы наших клиентов',
    'site_version'            => 'Версия сайта',
    'media_title'             => 'Мы выступаем экспертами в СМИ',
];
